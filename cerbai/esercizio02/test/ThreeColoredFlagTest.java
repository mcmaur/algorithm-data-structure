package cerbai.esercizio02.test;


import static org.junit.Assert.*;

import org.junit.Test;

import cerbai.esercizio02.ThreeColoredFlag;

public class ThreeColoredFlagTest {
	
	@Test
	public void testCheckColorsNull() {
		int [] a = null;
		assertFalse(ThreeColoredFlag.checkColors(a));	
	}
	
	@Test
	public void testCheckColorsArrayEmpty() {
		int [] a = new int [0];
		assertFalse(ThreeColoredFlag.checkColors(a));	
	}

	@Test
	public void testCheckColors() {
		int [] a = {1,2,3,4,5,6,7,8};
		assertFalse(ThreeColoredFlag.checkColors(a));	
	}
	
	@Test
	public void testCheckColorsAlreadyOrdered() {
		int [] b = {3,6,9,12,4,7,10,8,5};
		assertTrue(ThreeColoredFlag.checkColors(b));
	}

	
	@Test(expected=IllegalArgumentException.class)
	public void testSeparateColorsNull() {
		int [] a = null;
		ThreeColoredFlag.separateColors(a);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSeparateColorsEmpty() {
		int [] a = new int [0];
		ThreeColoredFlag.separateColors(a);
	}
	
	@Test
	public void testSeparateColors() {
		int [] a = {1,2,3,4,5,6,7,8};
		ThreeColoredFlag.separateColors(a);
		assertTrue(ThreeColoredFlag.checkColors(a));
	}
	
	@Test
	public void testSeparateColorsAlreadyOrdered() {
		int [] b = {3,6,9,12,4,7,10,8,5};
		ThreeColoredFlag.separateColors(b);
		assertTrue(ThreeColoredFlag.checkColors(b));
	}
	
	@Test
	public void testSeparateColorsRandomValues() {
		int [] c = new int [5];
		for(int i=0;i<c.length;i++)
			c[i]=(int)(Math.random() * 100);
		ThreeColoredFlag.separateColors(c);
		assertTrue(ThreeColoredFlag.checkColors(c));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSeparateColorsAltNull() {
		int [] a = null;
		ThreeColoredFlag.separateColorsAlt(a);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testSeparateColorsAltEmpty() {
		int [] a = new int [0];
		ThreeColoredFlag.separateColorsAlt(a);
	}
	
	@Test
	public void testSeparateColorsAlt() {
		int [] a = {1,2,3,4,5,6,7,8};
		ThreeColoredFlag.separateColorsAlt(a);
		assertTrue(ThreeColoredFlag.checkColors(a));
	}
	
	@Test
	public void testSeparateColorsAltAlreadyOrdered() {
		int [] b = {3,6,9,12,4,7,10,8,5};
		ThreeColoredFlag.separateColorsAlt(b);
		assertTrue(ThreeColoredFlag.checkColors(b));
	}
	
	@Test
	public void testSeparateColorsAltRandomValues() {
		int [] c = new int [5];
		for(int i=0;i<c.length;i++)
			c[i]=(int)(Math.random() * 100);
		ThreeColoredFlag.separateColorsAlt(c);
		assertTrue(ThreeColoredFlag.checkColors(c));
	}

}
