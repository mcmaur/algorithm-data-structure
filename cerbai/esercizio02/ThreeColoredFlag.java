package cerbai.esercizio02;

public class ThreeColoredFlag {
	
	private ThreeColoredFlag(){
		//avoid the creation of the object
	}
	
	/**
	 * Stabilisce se l'array ha i colori divisi correttamente (postcondizione algoritmo raggiunta)
	 * @param array - array da controllare
	 * @return true postcondizione raggiunta, false altrimenti
	 */
	public static boolean checkColors(int[] array){
		if(array == null || array.length == 0) return false;
		//condizione vera se per ogni elemento dell'array quello che viene deve essere < del corrente
		for(int i = 0; i < array.length-1; i++) {
			if ((array[i]%3) > (array[(i+1)]%3))
				return false;
		}
		return true;
	}
	
	/**
	 * Realizza l'algoritmo della bandiera tricolore
	 * @param array - array da controllare
	 */
	 public static void separateColors(int[] array){
		 if(array == null || array.length == 0) throw new IllegalArgumentException();
		 int i = 0, j = 0, k = array.length-1;
		 while(j <= k){
			 if (array[j]%3 == 0){//verde
				 swap(array, i, j);
				 i++;
				 j++;
			 }
			 else if (array[j]%3 == 1){//bianco
				 j++;
			 }
			 else if (array[j]%3 == 2){//rosso
				 swap (array,k,j);
				 k--;
			 }
		 }
	 }
	 
	 /**
		 * Realizza l'algoritmo alternativo della bandiera tricolore
		 * @param array - array da controllare
		 * 
		 *  0         i              j            k
		 * |_ _ _ _ _| _ _ _ _ _ _ _ _| _ _ _ _ _ _| _ _ _ _ _|
		 *   verde     da esaminare      bianco       rosso
		 */
		 public static void separateColorsAlt(int[] array){
			 if(array == null || array.length == 0) throw new IllegalArgumentException();
			 int i = 0, j = array.length-1 , k = array.length-1;
			 while(i <= j){
				 //se l'elemento è verde posso lasciarlo dove e' ed aumento la sezione verde
				 if (array[i]%3 == 0){//verde
					 i++;
				 }
				 //se l'elemento è bianco lo scambio con l'ultimo da esaminare e aumento la sezione bianca
				 else if (array[i]%3 == 1){//bianco
					 swap (array, i, j);
					 j--;
				 }
				 //se l'elemento è rosso lo scambio con l'ultimo dei bianchi che devo avere la accortezza di
				 //		scambiare con l'ultimo da esaminare ed aumentare sia la seziona rossa che la bianca
				 else if (array[i]%3 == 2){//rosso
					 swap (array, i, j);
					 swap (array, k, j);
					 j--;
					 k--;
				 }
			 }
		 }
	 
	 /**
	  * Provvede ad eseguire lo scambio di due elemnti all interno dell array
	  * @param arr - array su cui fare lo scambio
	  * @param i - indice elemento da scambiare
	  * @param j - indice elemento da scambiare
	  */
	 private static void swap(int [] arr,int i,int j){
		 int temp = arr[i];
		 arr[i] = arr[j];
		 arr[j] = temp;
	 }
}