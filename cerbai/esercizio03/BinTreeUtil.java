 package cerbai.esercizio03;
import static java.lang.System.out;

public class BinTreeUtil {

  private BinTreeUtil() {}

  /**
  *Inserisce elem nell'albero t nel punto specificato dalla stringa path
  *@param elem - l'elemento da inserire
  *@param t - l'albero sul quale effettuare l'inserimento
  *@param path - Stringa indicante la posizione dell'inserimento
  *@return l'albero t modificato
  */
  public static BinTree add(int elem, BinTree t, String path) {
    if(t == null) return new BinTree(elem);
    
    if(path.length() == 0) t.element = elem;
    else {
    	switch(path.charAt(0)){
    		case 'L': 
    			t.left = add(elem, t.left, path.substring(1));
    			break;
    		case 'R': 
    			t.right = add(elem, t.right, path.substring(1));
    			break;
    		default:
    			throw new IllegalArgumentException("Character not supported");
    	}
    }
    return t;
  }


  /* NON FA PARTE DELL'ESERCIZIO. Potete usare questo metodo
    per visualizzare in modo semplice l'albero t sulla consolle,
    (ruotato di 90 gradi).
  */
  public static void display(BinTree t) {
    display(t, 0);
  }

  /* NON FA PARTE DELL'ESERCIZIO. Usato da display(BinTree t) */
  private static void display(BinTree t, int k) {
    if(t != null) {
      display(t.right, k+1);
      for(int i = 0; i < k; i++) out.print("   ");
      out.println(t.element);
      display(t.left, k+1);
    }
  }

  /**
   * Verifico se il nodo è una foglia
   * @param t - il nodo da verificare
   * @return true se è una foglia, false altrimentoi
   */
  public static boolean isLeaf(BinTree t) {
    return t.left == null && t.right == null;
  }

  /**
   * Stampa l'albero in preorder sulla consolle
   * @param t - l'albero di cui stampare i nodi
   */
  public static void preorderPrint(BinTree t) {
	if(t == null) return;
	System.out.print(t.element+";");
	preorderPrint(t.left);
	preorderPrint(t.right);
  }

  /**
   * Stampa l'albero in inorder sulla consolle
   * @param t - l'albero di cui stampare i nodi
   */
  public static void inorderPrint(BinTree t) {
	if(t == null) return;
	inorderPrint(t.left);
	System.out.print(t.element+";");
	inorderPrint(t.right);
  }

  /**
   * Stampa l'albero in postorder sulla consolle
   * @param t - l'albero di cui stampare i nodi
   */
  public static void postorderPrint(BinTree t) {
	  if(t == null) return;
	  postorderPrint(t.left);
	  postorderPrint(t.right);
	  System.out.print(t.element+";");
  }
  
  /**
   * Restituisce il numero di nodi di t
   * @param t - l'albero di cui contare i nodi
   * @return il numero di nodi, 0 se vuoto
   */
  public static int size(BinTree t) {
	return t == null? 0 : size(t.left)+1+size(t.right);
  }

  /**
   * Restituisce la somma dei valori di tutti gli elementi
   * @param t - l'albero di cui fare la somma dei valori dei nodi
   * @return la somma dei valori dei nodi, 0 se vuoto
   */
  public static int sum(BinTree t) {
	return t == null? 0 : t.element+sum(t.left)+sum(t.right);
  }

  /**
   * Restituisce l'altezza dell'albero
   * @param t - l'albero di cui contare l'altezza
   * @return il valore dell'altezza
   */
  public static int height(BinTree t) {
	  return t == null? 0 : Math.max(height(t.left),height(t.right))+1;
  }

  /**
   * Restituisce il valore massimo degli elementi
   * @param t - l'albero su cui trovare il massimo dei valori dei nodi
   * @return il massimo degli elemente, Integer.MIN_VALUE se albero vuoto
   */
  public static int maxElem(BinTree t) {
	return t==null? Integer.MIN_VALUE : Math.max(Math.max(maxElem(t.left),maxElem(t.right)),t.element);
  }

  /**
   * Restituisce il massimo dei pesi dei cammini dalla radice a una foglia se tutti i valori sono positivi
   * @param t - l'albero su cui effettuare calcolo del massimo cammino
   * @return valore del massimo cammino, 0 se albero vuoto
   */
  public static int maxPositivePath(BinTree t) {
	  if (t == null) return 0;
	  return Math.max(maxPositivePath(t.left)+t.element,maxPositivePath(t.right)+t.element);
  }

  /**
   * Restituisce il massimo dei pesi dei cammini dalla radice a una foglia anche con valori negativi
   * @param t - l'albero su cui effettuare calcolo del massimo cammino
   * @return valore del massimo cammino, Integer.MIN_VALUE se albero vuoto
   */
  public static int maxPath(BinTree t) {
	  return t == null? Integer.MIN_VALUE : maxPositivePath(t);
  }
  
  /**
   * Restituisce il numero delle foglie
   * @param t - l'albero di cui contare le foglie
   * @return il numero di foglie, 0 se l'albero è null
   */
  public static int numberOfLeaves(BinTree t) {
	  if (t == null) return 0;
	  return isLeaf(t)? 1 : numberOfLeaves(t.left)+numberOfLeaves(t.right);
  }

  /** 
   * Modifica l'albero incrementando di 1 i valori di tutti gli elementi
   * @param t - l'albero di cui incrementare i valori
   */
  public static void increment(BinTree t) {
	  if (t == null) return;
	  t.element = t.element+1;
	  increment(t.left);
	  increment(t.right);
  }
  
  /**
   * Modifica l'albero incrementando di 1 i valori di tutte le foglie 
   * @param t - l'albero di cui incrementare i valori delle foglie
   */
  public static void incrementLeaves(BinTree t) {
	  if (t == null) return;
	  if (isLeaf(t)) t.element = t.element+1;
	  else{
		  incrementLeaves(t.left);
		  incrementLeaves(t.right);
	  }
  }

  /**
   * Verifico la presenza dell'elemento x nell'albero t
   * @param x - elemento da cercare
   * @param t - albero all'interno del quale cercare
   * @return true se l'elemento è presente, false altrimenti
   */
  public static boolean search(int x, BinTree t) {
	  if(t == null) return false;
	  if(t.element == x) return true;
	  return search(x, t.left) || search(x, t.left);
  }

  /**
   * Restituisce un sottoalbero di t avente come radice x
   * @param x - elemento radice del sottoalbero restituito
   * @param t - albero sul quale effettuare la ricerca
   * @return sottoalbero di radice x, null se l'elemento non è presente nell'albero
   */
  public static BinTree find(int x, BinTree t) {
	  BinTree tmp = null, tmp2 = null;
	  if(t == null) return null;
	  if(t.element == x) return t;
	  if(t.left != null)
		  tmp = find(x,t.left);
	  if(t.right != null)
		  tmp2 = find(x,t.right);
	  if (tmp != null) return tmp;
	  if (tmp2 != null) return tmp2;
	  return null;
  }

  /**
   * Verifico se t1 e t2 hanno la stessa struttura e gli stessi valori
   * @param t1 - albero da confrontare
   * @param t2 - albero da confrontare
   * @return true se sono uguali, false altrimenti
   */
  public static boolean areEqual(BinTree t1, BinTree t2) {
	  if(t1 == null && t2 == null) return true;
	  if(t1 == null || t2 == null) return false;
	  if(t1.element == t2.element)
		  return areEqual(t1.left, t2.left) && areEqual(t1.right, t2.right);
	  return false;
  }
  
  /**
   * Crea e restituisce un nuovo albero copia di t 
   * @param t - albero da copiare
   * @return nuovo albero copia di t, null se t è null
   */
  public static BinTree copy(BinTree t) {
    return t == null? null : new BinTree(t.element,copy(t.left),copy(t.right));
  }
  
  /**
   * costruisce un nuovo albero speculare di t
   * @param t - albero da copiare specchiato
   * @return nuovo albero specchiato di t, null se t è null
   */
  public static BinTree mirrorCopy(BinTree t) {
    if(t == null) return null;
    return new BinTree(t.element, mirrorCopy(t.right), mirrorCopy(t.left));
  }

  /**
   * modifica l'albero t facendolo diventare il suo speculare
   * @param t - albero di cui creare lo speculare
   */
  public static void mirrorInPlace(BinTree t) {
	  if(t == null) return;
	  mirrorInPlace(t.left);
	  mirrorInPlace(t.right);
	  BinTree tmpR = t.right;
	  t.right = t.left;
	  t.left = tmpR;
  }

  /**
   * Restituisce la somma dei valori di tutti i nodi di un sottoalbero di t di radice x (x incluso)
   * @param x - elemento da ricercare
   * @param t - albero da esaminare
   * @return somma dei valori di un sottoalbero di radice x, 0 se x non è presente nell'albero
   */
  public static int sumDescendants(int x, BinTree t) {
	  BinTree tmp = find(x, t);
	  return sum(tmp);
  }

  /**
   * Costruisce un nuovo albero uguale a t fino al livello h-1 incluso
   * @param h - altezza alla quale effettuare il troncamento
   * @param t - albero da troncare
   * @return nuovo albero simile a t ma troncato al livello h
   */
  public static BinTree trimmed(int h, BinTree t){
	  if(t == null || h == 0) return null;
	  BinTree le = trimmed(h-1,t.left);
	  BinTree ri = trimmed(h-1,t.right);
	  return new BinTree(t.element,le,ri);
  }

  /**
   * Modifica l'albero t eliminando tutti i nodi di livello >= h,
   * @param h - altezza alla quale troncare
   * @param t - albero da troncare
   * @return l'albero t modificato
   */
  public static BinTree trim(int h, BinTree t){
	  if(t == null || h == 0) return t = null;
	  t.left = trim(h-1,t.left);
	  t.right = trim(h-1,t.right);
	  return t;
  }
  
  /**
   * Scrive sulla console i nodi-cardine dell'albero
   * @param t - albero su cui effettuare la ricerca dei nodi cardine
   */
  public static void printCentralNodes(BinTree t) {
		  printCentralNodes(0, t);
  }
  
  private static void printCentralNodes(int level,BinTree t) {
	  if(t == null) return;
	  if((BinTreeUtil.height(t)-1) == level)
		  System.out.print(t.element+";");
	  else{
		  printCentralNodes(level+1,t.left);
		  printCentralNodes(level+1,t.right);
	  }
  }

  /* 
    classe ausiliaria per l'esercizio successivo
   */
  private static class BoolIntPair {
    boolean isCB;
    int height;

    BoolIntPair(boolean isCB, int height) {
      this.isCB = isCB; this.height = height;
    }
  }

  /**
   * Verifico che l'albero t è completamente bilanciato
   * @param t - albero su cui effettuare la verifica
   * @return true se è bilanciato, false altrimenti
   */
  public static boolean isCompletelyBalancedFromTextbook(BinTree t) {
    return isCBHeight(t).isCB;
  }

  private static BoolIntPair isCBHeight(BinTree t) {
	  if(t == null) return new BoolIntPair(true,-1);
	  if(t.left == null && t.right == null) return new BoolIntPair(true,0);
	  if(t.left == null || t.right == null) return new BoolIntPair(false,0);
	  BoolIntPair left = isCBHeight(t.left);
	  BoolIntPair right = isCBHeight(t.right);
	  if (left.isCB && right.isCB) {
		  if(left.height == right.height)
			  return new BoolIntPair(true,left.height+1);
	  }
	  return new BoolIntPair(false,0);
  }

  /**
   * Verifico che l'albero t è completamente bilanciato (versione "snella")
   * @param t - albero su cui effettuare la verifica
   * @return true se è bilanciato, false altrimenti
   */
  public static boolean isCompletelyBalanced(BinTree t) {
    return heightCB(t) != -2;
  }
  
  private static int heightCB(BinTree t) {
	  if(t == null) return -1;
	  int l = heightCB(t.left);
	  int r = heightCB(t.right);
	  if(l == -2 || r == -2 || l-r != 0) return -2;
	  return l+1;
  }

  /* 
    classe ausiliaria per l'esercizio successivo
   */
  protected static class IntPair {
    int height;
    int maxUnbal;

    IntPair(int h, int u) {
      height = h; maxUnbal = u;
    }
  }

  /**
   * restituisce il massimo sbilanciamento dei nodi di un albero
   * @param t - albero su cui calcolare il massimo sbilanciamento
   * @return il massimo sbilanciamento
   */
  public static int maxUnbalance(BinTree t) {
    return heightUnbalance(t).maxUnbal;
  }

  /*
   * nello stile del libro di testo:
   * restituisce una coppia di interi di cui il primo
   * è l'altezza di t, il secondo è il massimo sbilanciamento
   * dei sottoalberi di t, dove si definisce:
   * sbilanciamento di un albero = valore assoluto della differenza fra
   * l'altezza del sottoalbero sinistro e l'altezza del sottoalbero destro
   */
  private static IntPair heightUnbalance(BinTree t) {
    if(t == null) return new IntPair(-1,0);
    	IntPair l = heightUnbalance(t.left);
    	IntPair r = heightUnbalance(t.right);
    	int alt = Math.max(l.height, r.height) + 1;
    	int maxU = Math.max(Math.max(r.maxUnbal,l.maxUnbal),Math.abs(l.height - r.height));
    	return new IntPair(alt, maxU);
    }

  /**
   * Verifica se l'albero t è "1-bilanciato",
   * @param t - albero su cui effettuare la verifica
   * @return true se è 1-bilanciato, false altrimenti
   */
  public static boolean is1Balanced(BinTree t) {
	  return is1BalaInt(t) != -2;
  }
  
  private static int is1BalaInt(BinTree t){
	  if(t == null) return -1;
	  int l = is1BalaInt(t.left);
	  int r = is1BalaInt(t.right);
	  if(l == -2 || r == -2 || Math.abs(l - r) > 1) return -2;
	  return Math.max(l, r)+1;
  }
  
}

