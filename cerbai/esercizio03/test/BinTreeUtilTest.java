package cerbai.esercizio03.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio03.BinTree;
import cerbai.esercizio03.BinTreeUtil;

public class BinTreeUtilTest {
	BinTree albero;
	private ByteArrayOutputStream outBuffer = new ByteArrayOutputStream ( ) ;
	private ByteArrayOutputStream errBuffer = new ByteArrayOutputStream ( ) ;
	
	@Before
	public void setupOutputStreams () {
		BinTree rl = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rr = new BinTree(5,new BinTree(-1),new BinTree(2));
		albero = new BinTree(3,rl,rr);
		System.setOut(new PrintStream (outBuffer)) ;
		System.setErr(new PrintStream (errBuffer)) ;
	}
	
	@After
	public void cleanupOutputStreams ( ) {
		System.setOut(null);
		System.setErr(null);
	}
	
	/*************************************/
	public void disegna(BinTree b){
		BinTree.draw(b, "Albero");
		try {
			Thread.sleep(22000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	/*************************************/
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddErrorCharacter() {
		BinTreeUtil.add(9, albero, "RRXRLLR");
		assertEquals(BinTreeUtil.size(albero),8);
	}
	
	@Test
	public void testAdd() {
		BinTreeUtil.add(9, albero, "RRLRLLR");
		assertEquals(BinTreeUtil.size(albero),8);
	}
	
	@Test
	public void testAddPathEmpty() {
		BinTreeUtil.add(100, albero, "");
		BinTreeUtil.preorderPrint(albero);
		assertEquals("100;7;2;1;5;-1;2;",outBuffer.toString());
	}
	
	@Test
	public void testAddPathTooLong() {
		BinTreeUtil.add(100, albero, "RRRRRRRRRRRRRRRRRRRR");
		BinTreeUtil.preorderPrint(albero);
		assertEquals("3;7;2;1;5;-1;2;100;",outBuffer.toString());
	}

	@Test
	public void testPreorderPrint() {
		BinTreeUtil.preorderPrint(albero);
		assertEquals("3;7;2;1;5;-1;2;",outBuffer.toString());
	}
	
	@Test
	public void testPreorderPrintNull() {
		BinTreeUtil.preorderPrint(null);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testInorderPrint() {
		BinTreeUtil.inorderPrint(albero);
		assertEquals("2;7;1;3;-1;5;2;",outBuffer.toString());
	}
	
	@Test
	public void testInorderPrintNull() {
		BinTreeUtil.inorderPrint(null);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testPostorderPrint() {
		BinTreeUtil.postorderPrint(albero);
		assertEquals("2;1;7;-1;2;5;3;",outBuffer.toString());
	}
	
	@Test
	public void testPostorderPrintNull() {
		BinTreeUtil.postorderPrint(null);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testSize() {
		assertEquals(BinTreeUtil.size(albero),7);
	}
	
	@Test
	public void testSizeNull() {
		assertEquals(BinTreeUtil.size(null),0);
	}

	@Test
	public void testSum() {
		assertEquals(BinTreeUtil.sum(albero),(2+1+7+5+3+2+(-1)));
	}
	
	@Test
	public void testSumNull() {
		assertEquals(BinTreeUtil.sum(null),(0));
	}

	@Test
	public void testHeight() {
		assertEquals(BinTreeUtil.height(albero),3);
	}
	
	@Test
	public void testHeightNull() {
		assertEquals(BinTreeUtil.height(null),0);
	}

	@Test
	public void testMaxElem() {
		assertEquals(BinTreeUtil.maxElem(albero),7);
	}
	
	@Test
	public void testMaxElemNull() {
		assertEquals(BinTreeUtil.maxElem(null),Integer.MIN_VALUE);
	}
	
	@Test
	public void testMaxPositivePath() {
		BinTree rl = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rr = new BinTree(5);
		BinTree alb = new BinTree(3,rl,rr);
		assertEquals(BinTreeUtil.maxPositivePath(alb),12);
	}
	
	@Test
	public void testMaxPositivePathNull() {
		assertEquals(BinTreeUtil.maxPositivePath(null),0);
	}

	@Test
	public void testMaxPath() {
		assertEquals(BinTreeUtil.maxPath(albero),12);
		
	}
	
	@Test
	public void testMaxPathNull() {
		assertEquals(BinTreeUtil.maxPath(null),Integer.MIN_VALUE);
		
	}

	@Test
	public void testNumberOfLeaves() {
		assertEquals(BinTreeUtil.numberOfLeaves(albero),4);
	}
	
	@Test
	public void testNumberOfLeavesNull() {
		assertEquals(BinTreeUtil.numberOfLeaves(null),0);
	}

	@Test
	public void testIncrement() {
		BinTreeUtil.increment(albero);
		BinTreeUtil.preorderPrint(albero);
		assertEquals("4;8;3;2;6;0;3;",outBuffer.toString());
	}
	
	@Test
	public void testIncrementNull() {
		BinTree n = null;
		BinTreeUtil.increment(n);
		BinTreeUtil.preorderPrint(n);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testIncrementLeaves() {
		BinTreeUtil.incrementLeaves(albero);
		BinTreeUtil.preorderPrint(albero);
		assertEquals("3;7;3;2;5;0;3;",outBuffer.toString());
	}
	
	@Test
	public void testIncrementLeavesNull() {
		BinTree n = null;
		BinTreeUtil.incrementLeaves(n);
		BinTreeUtil.preorderPrint(n);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testSearchValuePresent() {
		assertTrue(BinTreeUtil.search(7,albero));
	}
	
	@Test
	public void testSearchValueNotPresent() {
		assertFalse(BinTreeUtil.search(4,albero));
	}
	
	@Test
	public void testSearchValueNull() {
		assertFalse(BinTreeUtil.search(4,null));
	}

	@Test
	public void testFindValuePresent() {
		BinTree t = BinTreeUtil.find(7,albero);
		BinTreeUtil.preorderPrint(t);
		assertEquals("7;2;1;",outBuffer.toString());
		
	}
	
	@Test
	public void testFindValueNotPresent() {
		BinTree t = BinTreeUtil.find(4,albero);
		assertNull(t);
		
	}
	
	@Test
	public void testFindNull() {
		BinTree t = BinTreeUtil.find(4,null);
		assertNull(t);
		
	}

	@Test
	public void testAreEqualBothNull() {
		assertTrue(BinTreeUtil.areEqual(null, null));
	}
	
	@Test
	public void testAreEqualOneNull() {
		assertFalse(BinTreeUtil.areEqual(albero, null));
	}
	
	@Test
	public void testAreEqualTrue() {
		BinTree rl = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rr = new BinTree(5,new BinTree(-1),new BinTree(2));
		BinTree t2 = new BinTree(3,rl,rr);
		assertTrue(BinTreeUtil.areEqual(albero, t2));
	}
	
	@Test
	public void testAreEqualFalse() {
		BinTree rl = new BinTree(3,new BinTree(5),new BinTree(1));
		BinTree rr = new BinTree(5,new BinTree(1),new BinTree(2));
		BinTree t2 = new BinTree(3,rl,rr);
		assertFalse(BinTreeUtil.areEqual(albero, t2));
	}

	@Test
	public void testCopy() {
		BinTree t = BinTreeUtil.copy(albero);
		BinTreeUtil.preorderPrint(t);
		assertEquals("3;7;2;1;5;-1;2;",outBuffer.toString());
	}
	
	@Test
	public void testCopyNull() {
		BinTree t = BinTreeUtil.copy(null);
		BinTreeUtil.preorderPrint(t);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testMirrorCopy() {
		BinTree t = BinTreeUtil.mirrorCopy(albero);
		BinTreeUtil.preorderPrint(t);
		assertEquals("3;5;2;-1;7;1;2;",outBuffer.toString());
	}
	
	@Test
	public void testMirrorCopyNull() {
		BinTree t = BinTreeUtil.mirrorCopy(null);
		BinTreeUtil.preorderPrint(t);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testMirrorInPlace() {
		BinTreeUtil.mirrorInPlace(albero);
		BinTreeUtil.preorderPrint(albero);
		assertEquals("3;5;2;-1;7;1;2;",outBuffer.toString());
	}
	
	@Test
	public void testMirrorInPlaceNull() {
		BinTree ut = null;
		BinTreeUtil.mirrorInPlace(ut);
		BinTreeUtil.preorderPrint(ut);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testSumDescendants() {
		assertEquals(10,BinTreeUtil.sumDescendants(7, albero));
	}
	
	@Test
	public void testSumDescendantsNull() {
		assertEquals(0,BinTreeUtil.sumDescendants(7, null));
	}

	@Test
	public void testTrimmed() {
		BinTree t = BinTreeUtil.trimmed(1,albero);
		BinTreeUtil.preorderPrint(t);
		assertEquals("3;",outBuffer.toString());
	}
	
	@Test
	public void testTrimmedNull() {
		BinTree t = BinTreeUtil.trimmed(1,null);
		BinTreeUtil.preorderPrint(t);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testTrim() {
		BinTree t = BinTreeUtil.trim(1,albero);
		BinTreeUtil.preorderPrint(t);
		assertEquals("3;",outBuffer.toString());
	}
	
	@Test
	public void testTrimNull() {
		BinTree t = BinTreeUtil.trim(1,null);
		BinTreeUtil.preorderPrint(t);
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testPrintCentralNodesNormalTree() {
		BinTreeUtil.printCentralNodes(albero);
		assertEquals("7;5;",outBuffer.toString());
	}
	
	@Test
	public void testPrintCentralNodesUnbalancedTree() {
		BinTree rl = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rr = new BinTree(5,null,null);
		BinTree alber = new BinTree(3,rl,rr);
		BinTreeUtil.printCentralNodes(alber);
		assertEquals("7;",outBuffer.toString());
	}
	
	@Test
	public void testPrintCentralNodesHeight2() {
		BinTree rl = new BinTree(7,null,null);
		BinTree rr = new BinTree(5,null,null);
		BinTree alber = new BinTree(3,rl,rr);
		BinTreeUtil.printCentralNodes(alber);
		assertEquals("",outBuffer.toString());
	}
	
	@Test
	public void testPrintCentralNodesNull() {
		BinTreeUtil.printCentralNodes(null);
		assertEquals("",outBuffer.toString());
	}
	
	@Test
	public void testIsCompletelyBalancedFromTextbookNull() {
		assertTrue(BinTreeUtil.isCompletelyBalancedFromTextbook(null));
	}

	@Test
	public void testIsCompletelyBalancedFromTextbookTrue() {
		assertTrue(BinTreeUtil.isCompletelyBalancedFromTextbook(albero));
	}
	
	@Test
	public void testIsCompletelyBalancedFromTextbookFalse() {
		BinTree rl = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rr = new BinTree(5,new BinTree(-1),null);
		BinTree alber = new BinTree(3,rl,rr);
		assertFalse(BinTreeUtil.isCompletelyBalancedFromTextbook(alber));
	}
	
	@Test
	public void testIsCompletelyBalanced() {
		assertTrue(BinTreeUtil.isCompletelyBalanced(albero));
	}
	
	@Test
	public void testIsCompletelyBalancedFalse() {
		BinTree rl = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rr = new BinTree(5,new BinTree(-1),null);
		BinTree alber = new BinTree(3,rl,rr);
		assertFalse(BinTreeUtil.isCompletelyBalanced(alber));
	}

	@Test
	public void testMaxUnbalanceHeight2() {
		BinTree rl = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rr = new BinTree(5,new BinTree(-1),null);
		BinTree alber = new BinTree(3,rl,rr);
		assertEquals(1,BinTreeUtil.maxUnbalance(alber));
	}
	
	@Test	public void testMaxUnbalanceHeight4() {
		BinTree alber = new BinTree(3,null,null);
		BinTreeUtil.add(7, alber, "L");
		BinTreeUtil.add(5, alber, "R");
		BinTreeUtil.add(2, alber, "LL");
		BinTreeUtil.add(1, alber, "LR");
		BinTreeUtil.add(3, alber, "LLL");
		BinTreeUtil.add(9, alber, "LLR");
		assertEquals(2,BinTreeUtil.maxUnbalance(alber));
	}
	
	@Test
	public void testMaxUnbalanceNull() {
		assertEquals(0,BinTreeUtil.maxUnbalance(null));
	}
	
	@Test
	public void testMaxUnbalanceEmpty() {
		BinTree alber = new BinTree(3,null,null);
		assertEquals(0,BinTreeUtil.maxUnbalance(alber));
	}

	@Test
	public void testIs1BalancedNull() {
		assertTrue(BinTreeUtil.is1Balanced(null));
	}
	
	@Test
	public void testIs1Balanced1Bilanciato() {
		assertTrue(BinTreeUtil.is1Balanced(albero));
	}
	
	@Test
	public void testIs1BalancedNon1Bilanciato() {
		BinTree z = new BinTree(15,null,new BinTree(9));
		BinTree rr = new BinTree(7,new BinTree(2),new BinTree(1));
		BinTree rl = new BinTree(5,null,z);
		BinTree alber = new BinTree(3,rl,rr);
		assertFalse(BinTreeUtil.is1Balanced(alber));
	}

}
