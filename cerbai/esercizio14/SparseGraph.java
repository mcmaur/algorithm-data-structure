package cerbai.esercizio14;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;


public class SparseGraph <V extends Comparable <V>> implements Graph <V> {	
	private HashMap <V, LinkedList<V>> map;
	
	/**
	 * costruttore
	 * @param numnodi - numero di nodi del grafo
	 */
	public SparseGraph(int numnodi) {
		map = new HashMap <V,LinkedList<V>>(numnodi);
	}
	
	/**
	 * costruttore
	 * @param numnodi - numero di nodi del grafo
	 */
	public SparseGraph() {
		map = new HashMap <V,LinkedList<V>>();
	}

	/**
	 * Aggiunge il vertice vertex al grafo se il vertice non vi appartiene già
	 * @param vertex - vertice che si vuole inserire
	 * @return true se il vertice è stato inserito, false altrimenti
	 */
	@Override
	public boolean addVertex(V vertex) {
		//controllo che il vertice non sia nullo
		if(vertex == null) throw new IllegalArgumentException();
		//controllo se il vertice esiste già
		if(map.containsKey(vertex)) return false;
		//inizializzo la lista degli adiacenti e la inserisco nell'hashmap
		map.put(vertex, new LinkedList<V>());
		return true;
	}

	/**
	 * Aggiunge l' arco ( vertex 1, vertex 2) al grafo se l’arco non vi appartiene già.
	 * @throws IllegalArgumentException se uno dei due vertici non risulta appartenere al grafo
	 * @param vertex1 - uno degli estremi dell'arco
	 * @param vertex1 - uno degli estremi dell'arco
	 * @return true se l'arco viene inserito, false altrimenti
	 */
	@Override
	public boolean addEdge(V vertex1, V vertex2) throws IllegalArgumentException {
		//controllo che nella lista degli adiacenti di vertex1 sia presente o meno il vertex2
		if(hasEdge(vertex1, vertex2)) return false;
		//l'arco non è presente quindi lo aggiungo
		map.get(vertex1).add(vertex2);
		map.get(vertex2).add(vertex1);
		return true;
	}

	/**
	 * Verifico se il vertice dato è presente nel grafo
	 * @param vertex - vertice di cui verificare la presenza
	 * @return true se vertex fa parte del grafo, false altrimenti
	 */
	@Override
	public boolean hasVertex(V vertex) {
		return map.containsKey(vertex);
	}

	/**
	 * Verifico se l'arco dato è presente nel grafo
	 * @throws se uno dei due vertici non appartiene al grafo
	 * @param vertex1 - uno degli estremi dell'arco
	 * @param vertex2 - uno degli estremi dell'arco
	 * @return true se l’arco ( vertex1, vertex2) fa parte del grafo
	 */
	@Override
	public boolean hasEdge(V vertex1, V vertex2) throws IllegalArgumentException {
		//se uno dei due vertici non è presente lancio un eccezione
		if(!map.containsKey(vertex1) || !map.containsKey(vertex2)) throw new IllegalArgumentException();
		//controllo che nella lista degli adiacenti di vertex1 sia presente o meno il vertex2
		Iterator <V> iter = map.get(vertex1).iterator();
		while(iter.hasNext()) {
			//l'arco è presente
			if(iter.next() == vertex2) return true;
		}
		//l'arco non è presente
		return false;
	}

	/**
	 * Restituisce la lista dei vertici del grafo
	 * @return la lista dei vertici
	 */
	@Override
	public ArrayList<V> vertices() {
		return new ArrayList<V>(map.keySet());
	}

	/**
	 * Restituisce la lista di adiacenza del vertice dato
	 * @param vertex - vertice di cui cercare i vicini
	 * @return la lista dei vicini se il vertice appartiene al grafo, null altrimenti
	 */
	@Override
	public ArrayList<V> neighbors(V vertex) {
		if(!map.containsKey(vertex)) throw new IllegalArgumentException();
		return new ArrayList<V>(map.get(vertex));
	}

	/*
	 * Fornisce rappresentazione su console dei vertici e archi
	 */
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		Iterator <V> iterator = map.keySet().iterator();
		while (iterator.hasNext()) {
			V cur = iterator.next();
			String key = cur.toString();  
			String value = map.get(key).toString();
			str.append(key + " " + value+"\n");
		}
		str.append("\n");
		return str.toString();
	}
	
	/******************** FOR DEBUG PURPOSE **********************************************/
	/*
	 * stampo il grafo nel file dotGraph.txt
	 */
	public void print() {
		PrintStream output = setFileAsOutput("dotGraph");
		output.print("graph G {\n");
		toDot(output);
		output.print("}\n");
		
	}
	
	/*
	 * analizzo i vertici e gli archi e li trasformo in notazione dot
	 */
	private void toDot(PrintStream output) {
		Iterator <V> iterator = map.keySet().iterator();
		while (iterator.hasNext()) {
			V cur = iterator.next();
			String key = cur.toString();
			Iterator <V> iter = map.get(cur).iterator();
			while (iter.hasNext()) {
				V adj = iter.next();
				output.append(key + " -- "+adj+"\n");
			}
		}
	}
	
	/*
	 * apro il file e setto lo stream di output correttamente
	 */
	private static PrintStream setFileAsOutput(String filename) {
		FileOutputStream fp = null;
		try {
			fp = new FileOutputStream(System.getProperty("user.dir")+"/src/cerbai/esercizio14/"+filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return new PrintStream(fp);
	}

}
