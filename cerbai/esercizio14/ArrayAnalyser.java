package cerbai.esercizio14;

import java.util.ArrayList;

public class ArrayAnalyser <V> implements VertexAnalyser<V> {
public ArrayList <V> nodiVisitati;

	/**
	 * costruttore
	 */
	public ArrayAnalyser() {
		nodiVisitati = new ArrayList <V> ();
	}
	
	/**
	 * insirisce in un array i nodi man mano che vengono visitati
	 * @param vertex - nodo di cui si effettua la visita
	 */
	@Override
	public void analyse(V vertex) {
		nodiVisitati.add(vertex);
	}

}
