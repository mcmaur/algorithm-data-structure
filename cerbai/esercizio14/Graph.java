package cerbai.esercizio14;

import java.util.ArrayList;

public interface Graph <V extends Comparable <V>> {
	
	/*
	∗ Aggiunge il vertice vertex al grafo se il vertice non
	∗ vi appartiene già . Restituisce true se vertex è stato
	∗ aggiunto.
	*/
	boolean addVertex (V vertex);
	
	/*
	∗ Aggiunge l' arco ( vertex 1, vertex 2) al grafo se l’arco non
	∗ vi appartiene già . Solleva un’ eccezione se uno dei due
	∗ vertici non appartiene al grafo. Restituisce true se
	∗ l’ arco è stato aggiunto.
	*/
	boolean addEdge (V vertex1, V vertex2) throws IllegalArgumentException;
	
	/*
	∗ Restituisce true se vertex fa parte del grafo .
	*/
	boolean hasVertex (V vertex );
	
	/*
	∗ Restituisce true se l’arco ( vertex1, vertex2) fa
	∗ parte del grafo. Solleva un ’ eccezione se uno dei due
	∗ vertici non appartiene al grafo.
	*/
	boolean hasEdge (V vertex1, V vertex2) throws IllegalArgumentException;

	/*
	∗ Restituisce la lista dei vertici del grafo.
	*/
	ArrayList <V> vertices ( );
	
	/*
	∗ Restituisce la lista di adiacenza del vertice vertex
	∗( o null se il vertice non appartiene al grafo) .
	*/
	ArrayList <V> neighbors (V vertex);

}
