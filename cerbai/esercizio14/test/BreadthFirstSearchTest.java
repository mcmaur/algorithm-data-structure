package cerbai.esercizio14.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio14.ArrayAnalyser;
import cerbai.esercizio14.BreadthFirstSearch;
import cerbai.esercizio14.SparseGraph;

public class BreadthFirstSearchTest {
	SparseGraph <String> g;
	ArrayAnalyser <String> analyzer;
	BreadthFirstSearch <String> bfs;
	
	@Before
	public void setupOutputStreams () {
		g = new SparseGraph <String> ();
		g.addVertex("Torino");
		g.addVertex("Milano");
		g.addVertex("Roma");
		g.addVertex("Napoli");
		g.addVertex("Bari");
		g.addVertex("Palermo");
		g.addEdge("Torino","Milano");
		g.addEdge("Torino","Roma");
		g.addEdge("Roma","Milano");
		g.addEdge("Roma","Napoli");
		g.addEdge("Roma","Bari");
		g.addEdge("Napoli","Palermo");
		analyzer = new ArrayAnalyser <String> ();
		bfs = new BreadthFirstSearch <String> ();
	}

	@Test
	public void testSearch() {
		bfs.search(g, "Torino", analyzer);
		ArrayList <String> result = new ArrayList <String> ( Arrays.asList("Torino", "Milano", "Roma", "Napoli", "Bari", "Palermo"));
		assertEquals(result,analyzer.nodiVisitati);
	}

}
