package cerbai.esercizio14.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio14.ArrayAnalyser;
import cerbai.esercizio14.DepthFirstSearch;
import cerbai.esercizio14.SparseGraph;

public class DepthFirstSearchTest {
	SparseGraph <String> g;
	ArrayAnalyser <String> analyzer;
	DepthFirstSearch <String> dfs;
	
	@Before
	public void setupOutputStreams () {
		g = new SparseGraph <String> ();
		g.addVertex("Torino");
		g.addVertex("Milano");
		g.addVertex("Roma");
		g.addVertex("Napoli");
		g.addVertex("Bari");
		g.addVertex("Palermo");
		g.addEdge("Torino","Milano");
		g.addEdge("Torino","Roma");
		g.addEdge("Roma","Milano");
		g.addEdge("Roma","Napoli");
		g.addEdge("Roma","Bari");
		g.addEdge("Napoli","Palermo");
		analyzer = new ArrayAnalyser <String> ();
		dfs = new DepthFirstSearch <String> ();
	}

	@Test
	public void testSearch() {
		dfs.search(g, "Torino", analyzer);
		ArrayList <String> result = new ArrayList <String> ( Arrays.asList("Torino", "Roma", "Bari", "Napoli", "Palermo", "Milano"));
		assertEquals(result,analyzer.nodiVisitati);
	}

}
