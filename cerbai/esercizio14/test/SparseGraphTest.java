package cerbai.esercizio14.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio14.SparseGraph;

public class SparseGraphTest {
	SparseGraph <String> g;
	private ByteArrayOutputStream outBuffer = new ByteArrayOutputStream ( ) ;
	private ByteArrayOutputStream errBuffer = new ByteArrayOutputStream ( ) ;
	
	@Before
	public void setupOutputStreams () {
		g = new SparseGraph <String> ();
		System.setOut(new PrintStream (outBuffer)) ;
		System.setErr(new PrintStream (errBuffer)) ;
	}
	
	@After
	public void cleanupOutputStreams ( ) {
		System.setOut(null);
		System.setErr(null);
	}

	@Test
	public void testAddVertex() {
		g.addVertex("Palermo");
		assertTrue(g.hasVertex("Palermo"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddVertexArgumentNull() {
		g.addVertex(null);
	}
	
	@Test
	public void testAddEdge() {
		g.addVertex("Palermo");
		g.addVertex("Torino");
		g.addEdge("Palermo","Torino");
		assertTrue(g.hasEdge("Palermo","Torino"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testAddEdgeVertexNotPresent() {
		g.addVertex("Palermo");
		g.addEdge("Palermo","Torino");
	}
	
	@Test
	public void testHasVertex() {
		g.addVertex("Imperia");
		assertTrue(g.hasVertex("Imperia"));
	}
	
	@Test
	public void testHasVertexNotPresent() {
		assertFalse(g.hasVertex("Imperia"));
	}
	
	@Test
	public void testHasEdge() {
		g.addVertex("Bologna");
		g.addVertex("Venezia");
		g.addEdge("Bologna", "Venezia");
		assertTrue(g.hasEdge("Bologna", "Venezia"));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testHasEdgeVertexNotPresent() {
		g.addEdge("Venezia", "Torino");
	}
	
	@Test
	public void testVertices() {
		g.addVertex("Bologna");
		g.addVertex("Venezia");
		g.addVertex("Imperia");
		ArrayList <String> result = new ArrayList <String> ( Arrays.asList("Venezia","Imperia","Bologna"));
		assertEquals(result, g.vertices());
	}
	
	@Test
	public void testVerticesEmpty() {
		assertEquals(new ArrayList<String>(), g.vertices());
	}
	
	@Test
	public void testNeighbors() {
		g.addVertex("Bologna");
		g.addVertex("Venezia");
		g.addVertex("Imperia");
		g.addEdge("Venezia", "Imperia");
		g.addEdge("Venezia", "Bologna");
		ArrayList <String> result = new ArrayList <String> ( Arrays.asList("Imperia","Bologna"));
		assertEquals(result, g.neighbors("Venezia"));
	}
	
	@Test
	public void testNeighborsEmpty() {
		g.addVertex("Bologna");
		g.addVertex("Venezia");
		g.addVertex("Imperia");
		assertEquals(new ArrayList <String>(), g.neighbors("Venezia"));
	}
}
