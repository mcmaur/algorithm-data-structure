package cerbai.esercizio14;

public interface GraphSearch <V extends Comparable<V>> {

	void search ( Graph <V> graph, V source, VertexAnalyser <V> analyser);
	
}
