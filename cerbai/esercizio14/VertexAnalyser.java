package cerbai.esercizio14;

public interface VertexAnalyser <V> {

	void analyse (V vertex);

}
