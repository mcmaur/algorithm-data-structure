package cerbai.esercizio14;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Queue;


public class BreadthFirstSearch <V extends Comparable <V>> implements GraphSearch <V> {
	private Queue <V> coda;
	private HashMap <V,Boolean> visitato;
	
	/**
	 * metodo per effettuare una visita per ampiezza di tutti i nodi
	 * @param graph - grafo di cui esplorare i nodi
	 * @param source - nodo di partenza
	 * @param analyser - classe che conterrà il reale metodo di visita
	 */
	@Override
	public void search(Graph <V> graph, V source, VertexAnalyser<V> analyser) {
		//costruito l'hasmap che indica se il nodo è stato visitato o meno
		visitato = new HashMap <V,Boolean>();
		//costruisco la coda vuota
		coda =  new ArrayDeque <V>();
		//segnalo il nodo iniziale come visitato
		visitato.put(source, true);
		//accoda il nodo iniziale nella coda
		coda.add(source);
		//ciclo fino a che non è la coda non è vuota
		while(!coda.isEmpty()) {
			V nodoCorrente = coda.poll();
			//chiamo la vera funzione di visita
			analyser.analyse(nodoCorrente);
			ArrayList <V> adjacent = graph.neighbors(nodoCorrente);
			//per ogni nodo adiacente al nodo corrente
			for(V adj : adjacent) {
				//se non è già stato visitato
				if(!visitato.containsKey(adj) || !visitato.get(adj)) {
					//segnalo che il nodo è visitato
					visitato.put(adj,true);
					//inserisco tale nodo in coda
					coda.add(adj);
				}
			}
		}	
	}
}
