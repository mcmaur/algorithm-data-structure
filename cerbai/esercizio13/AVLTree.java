package cerbai.esercizio13;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

public class AVLTree <K extends Comparable <K>,T> implements Dictionary<K,T> {		
	private AVLNode root; //il nodo radice
	private int num_nodi;
	
	/**
	 * costruttore
	 */
	public AVLTree() {
	    root = null;
	    num_nodi = 0;
	  }

	/**
	 * restituisce il valore associato alla chiave
	 * @param key - chiave da cercare
	 * @return il valore associato alla chiave data
	 */
	@Override
	public T get(K key) {
		AVLNode n = getRecursive(key, root);
		if(n==null) throw new IllegalArgumentException();
		return n.value;
	}

	private AVLNode getRecursive(K key, AVLNode q) {
		if(q == null) return null;
		if(key.compareTo(q.key) == 0) return q;
		if(key.compareTo(q.key) < 0) return getRecursive(key, q.left);
		if(key.compareTo(q.key) > 0) return getRecursive(key, q.right);
		return null;
	}

	/**
	 * restituisce un booleano indicante se l'albero contiene o meno elementi
	 * @return false se albero ha almeno un elemento, true altrimenti
	 */
	@Override
	public boolean isEmpty() {
		return root == null;
	}

	/**
	 * rimuove il nodo associato alla chiave data e restituisce il valore di esso
	 * @param key - chiave da cercare
	 * @return il valore associato alla chiave data
	 */
	@Override
	public T remove(K key) {
		AVLNode cercato = getRecursive(key,this.root);
		if(cercato == null) return null;
		AVLNode sostituto, genitoreCercato = cercato.parent;
		if(cercato.right != null) {//il nodo ha figlio dx
			sostituto = minimum(cercato.right);
		} else {//il nodo non ha figlio dx
			if(cercato.left != null) {//il nodo ha figlio sx
				sostituto = cercato.left;
			} else {//il nodo è foglia
				sostituto = null;
			}
		}
		if(sostituto != null) {
			//elimino riferimento al nodo vecchio dal padre
			if(sostituto.parent.left == sostituto)
				sostituto.parent.left = null;
			else
				sostituto.parent.right = null;
			//aggiorno i riferimenti del sx e dx (e relativi genitori) dei figli del vecchio nodo
			sostituto.parent = genitoreCercato;
			cercato.left.parent = sostituto;
			sostituto.left = cercato.left;
			cercato.right.parent = sostituto;
			sostituto.right = cercato.right;
		}
		//il nodo da eliminare è la radice
		if(genitoreCercato == null)
			this.root = sostituto;
		else {
			//aggiorno i riferimenti dal genitore al nuovo nodo
			if(genitoreCercato.left == cercato)
				genitoreCercato.left = sostituto;
			else
				genitoreCercato.right = sostituto;
		}
		num_nodi--;
		/*aggiorno le altezze*/
		if(sostituto != null) {
			sostituto.h = 1 + Math.max(h(sostituto,'l'), h(sostituto,'r'));
			AVLNode node = sostituto;
			//risalgo ad aggiornare le altezze nei genitori e verifica gli sbilanciamenti
			while(node.parent != null) {
				node.parent.h = 1 + Math.max(h(node.parent,'l'), h(node.parent,'r'));
				identifyRotation(node.parent);
				node = node.parent;
			}
		}
		return cercato.value;
	}

	private AVLNode minimum(AVLNode nd) {
		while(nd.left != null) {
			nd = nd.left;
		}
		return nd;
	}

	/**
	 * restituisce il numero dei nodi dell'albero
	 * @return numero nodi dell'albero
	 */
	@Override
	public int size() {
		return num_nodi;
	}

	/**
	  * Funzione per inserire un nodo nell'albero
	  * @param key - chiave del nuovo elemento da inserire
	  * @param value - valore del nuovo elemento da inserire
	  */
	@Override
	public T put(K key, T value) {
		root = insertAVL(key, value, this.root, null);
		return null;
	}

	private AVLNode insertAVL(K key, T value, AVLNode q, AVLNode par) {
		/*se non vi è nessun nodo ne inserisco uno di valore key, value e parent (h=0 per default). 
		 * Se fosse la radice inserisco al posto di root in nuovo nodo con valore di parent null.*/
		if(q == null) {
			num_nodi++;
			return new AVLNode(key, value, par);
		}
		if(key.compareTo(q.key) == 0) {
			q.value = value;
		} else {
			if(key.compareTo(q.key) < 0)
				q.left = insertAVL(key, value, q.left, q);
			else
				q.right = insertAVL(key, value, q.right, q);
			//assegno il valore dell'altezza verificando l'altezze dei figli
			q.h = 1 + Math.max(h(q,'l'), h(q,'r'));
			//individua rotazione opportuna ed eseguila
			q = identifyRotation(q);
		}
		return q;
	}

	/**
	 * restituisce l'altezza del nodo, -1 se nodo null
	 * @param q - nodo padre
	 * @param c - lettera l per left, r per right
	 */
	private int h(AVLNode q, char c) {
		switch (c) {
		  case 'l':
			  if(q.left != null) return q.left.h;
			  else return -1;
		  case 'r':
			  if(q.right != null) return q.right.h;
			  else return -1;
		  default :
			  throw new IllegalArgumentException();
		}
	}

	/**
	  * Identifica e chiama il metodo appropriato per ribilanciare
	  * @param cur : Il nodo di cui si è verificato essere sbilanciato
	  * @return la nuova radice dell'albero modificato (se necessario modifica)
	  */
	private AVLNode identifyRotation(AVLNode cur) {
		if(h(cur,'l') - h(cur,'r') == 2) {
			if(h(cur.left,'l') >= h(cur.left,'r')) cur = rotateSS(cur);//caso SS : left-rotation
			else cur = rotateSD(cur);//caso SD = DD(a) + SS(c)
		} else if (h(cur,'l') - h(cur,'r')  == -2) {
			if(h(cur.right,'r') >= h(cur.right,'l')) cur = rotateDD(cur);//caso DD : right-rotation
			else cur = rotateDS(cur);//caso DS = SS(a) + DD(c)
		}
		return cur;
	}
	
	/**
	  * Rotazione SS del nodo in parametro.
	  * @param cur - Il nodo corrente per la rotazione
	  * @return la nuova radice dell'albero modificato
	  */
	private AVLNode rotateSS(AVLNode cur) {
		AVLNode b = cur;
		AVLNode a = cur.left;
		AVLNode SD = a.right;

		if(b.parent == null) { //b è la radice
			a.parent = null;
			this.root = a;
		} else {
			a.parent = b.parent;
		}
		b.parent = a;
		a.right = b;
		if(SD != null)//SD può essere nullo
			SD.parent = b;
		b.left = SD;
		// imposto le altezza ai valori corretti
		b.h = 1 + Math.max(h(b,'l'), h(b,'r'));
		a.h = 1 + Math.max(h(a,'l'), h(a,'r'));
		AVLNode node = a;
		while(node.parent != null) {
			node.parent.h = 1 + Math.max(h(node.parent,'l'), h(node.parent,'r'));
			node = node.parent;
		}
		return a;
	}
	
	/**
	  * Rotazione DD del nodo in parametro.
	  * @param cur - Il nodo corrente per la rotazione
	  * @return la nuova radice dell'albero modificato
	  */
	private AVLNode rotateDD(AVLNode cur) {		
		AVLNode b = cur;
		AVLNode a = cur.right;
		AVLNode DS = a.left;

		if(b.parent == null) { //b è la radice
			a.parent = null;
			this.root = a;
		} else {
			a.parent = b.parent;
		}
		b.parent = a;
		a.left = b;
		if(DS != null)//DS può essere nullo
			DS.parent = b;
		b.right = DS;
		// imposto le altezza ai valori corretti
		b.h= 1 + Math.max(h(b,'l'), h(b,'r'));
		a.h = 1 + Math.max(h(a,'l'), h(a,'r'));
		AVLNode node = a;
		while(node.parent != null) {
			node.parent.h = 1 + Math.max(h(node.parent,'l'), h(node.parent,'r'));
			node = node.parent;
		}
		return a;
	}
	
	/**
	  * Rotazione SD del nodo in parametro.
	  * @param cur - Il nodo corrente per la rotazione
	  * @return la nuova radice dell'albero modificato
	  */
	private AVLNode rotateSD(AVLNode cur) {
		cur.left = rotateDD(cur.left);
		return rotateSS(cur);
	}
	
	/**
	  * Rotazione DS del nodo in parametro.
	  * @param cur - Il nodo corrente per la rotazione
	  * @return la nuova radice dell'albero modificato
	  */
	private AVLNode rotateDS(AVLNode cur) {
		cur.right = rotateSS(cur.right);
		return rotateDD(cur);
	}

/******************** FOR DEBUG PURPOSE **********************************************/
	/*
	 * stampo l'albero l'albero nel file dotTree.txt
	 */
	public void print() {
		PrintStream output = setFileAsOutput("dotTree");
		output.print("digraph G {\n");
		toDot(output, this.root, 0);
		output.print("}\n");
		
	}
	
	/*
	 * analizzo ricorsivamente i nodi e li trasformo in notazione dot
	 */
	private void toDot(PrintStream output, AVLNode n, int counter) {
		if (n == null) return;
		if(counter == 0) {
			int sbil = h(n,'l') - h(n,'r');
			if (sbil < 2 && sbil > -2)
				output.print("nodo"+n.key+"[shape=record, label=\"{"+n.key+"|{h: "+n.h+"|s: "+sbil+"}}\"];\n");
			else
				output.print("nodo"+n.key+"[shape=record, fontcolor=red, label=\"{"+n.key+"|{h: "+n.h+"|s: "+sbil+"}}\"];\n");
		}
		counter++;
		if(n.left != null) {
			int sbilL = h(n.left,'l') - h(n.left,'r');
			if (sbilL < 2 && sbilL > -2)
				output.print("nodo"+n.left.key+"[shape=record, label=\"{"+n.left.key+"|{h: "+n.left.h+"|s: "+sbilL+"}}\"];\n");
			else
				output.print("nodo"+n.left.key+"[shape=record, fontcolor=red, label=\"{"+n.left.key+"|{h: "+n.left.h+"|s: "+sbilL+"}}\"];\n");
			output.print("nodo"+n.key+" -> nodo"+n.left.key+" [label=\"l\"];\n");//left
			output.print("nodo"+n.left.key+" -> nodo"+n.left.parent.key+" [label=\"p\"];\n");//parent
			counter++;
		}
		if(n.right != null) {
			int sbilR = h(n.right,'l') - h(n.right,'r');
			if (sbilR < 2 && sbilR > -2)
				output.print("nodo"+n.right.key+"[shape=record, label=\"{"+n.right.key+"|{h: "+n.right.h+"|s: "+sbilR+"}}\"];\n");
			else
				output.print("nodo"+n.right.key+"[shape=record, fontcolor=red, label=\"{"+n.right.key+"|{h: "+n.right.h+"|s: "+sbilR+"}}\"];\n");
			output.print("nodo"+n.key+" -> nodo"+n.right.key+" [label=\"r\"];\n");//right
			output.print("nodo"+n.right.key+" -> nodo"+n.right.parent.key+" [label=\"p\"];\n");//parent
			counter++;
		}
		toDot(output, n.left, counter);
		toDot(output, n.right, counter);
	}
	
	/*
	 * apro il file e setto lo stream di output correttamente
	 */
	private static PrintStream setFileAsOutput(String filename) {
		FileOutputStream fp = null;
		try {
			fp = new FileOutputStream(System.getProperty("user.dir")+"/src/cerbai/esercizio13/"+filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return new PrintStream(fp);
	}
	
	/**
	 * printa su console l'albero nodo per nodo
	 */
	public void printInOrder() {
		if(root != null)
			printInOrder(root);
	}

	private void printInOrder(AVLNode node) {
		  if(node == null) return;
		  printInOrder(node.left);
		  System.out.print(node+"\n");
		  printInOrder(node.right);
	  }

/******************** INTERNAL CLASS NODE **********************************************/

	/** AVL-Node class **/
	private class AVLNode {
		private AVLNode left;
		private AVLNode right;
		private AVLNode parent;
		private K key;
		private int h;
		private T value;
		
		public AVLNode(K k, T obj,AVLNode par) {
			left = right = parent = null;
			h = 0;
			key = k;
			value = obj;
			parent = par;
		}

		@Override
		public String toString() {
			if(parent == null) {//radice
				if(left == null && right == null)//radice senza figli
					return "Radice/Foglia [key=" + key + ",left= null, right=null, h=" + h + ", value=" + value+ "]";
					if(left == null)//nodo con unico figlio right
						return "Radice [key=" + key + ",left=null, right=" + right.key + ", h=" + h + ", value=" + value+ "]";
					if(right == null)//nodo con unico figlio left
						return "Radice [key=" + key + ",left=" + left.key + ", right=null, h=" + h + ", value=" + value+ "]";
					if(left != null && right != null)//radice senza figli
						return "Radice [key=" + key + ",left=" + left.key + ", right="+right.key+", h=" + h + ", value=" + value+ "]";
			}
			if(left == null && right == null)//foglia
				return "Foglia [key=" + key + ", parent="+ parent.key + ", h=" + h + ", value=" + value+ "]";
			if(left == null && right != null)//nodo con unico figlio left
				return "AVLNode [key=" + key + ",left=null, right=" + right.key + ", parent="+ parent.key + ", h=" + h + ", value=" + value+ "]";
			if(left != null && right == null)//nodo con unico figlio right
				return"AVLNode [key=" + key + ",left=" + left.key + ", right=null, parent="+ parent.key + ", h=" + h + ", value=" + value+ "]";
			return "AVLNode [key=" + key + ",left=" + left.key + ", right=" + right.key + ", parent="+ parent.key + ", h=" + h + ", value=" + value
			+ "]";
		}
	}
	
}