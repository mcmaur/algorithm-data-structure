package cerbai.esercizio13.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio13.AVLTree;

public class AVLTreeTest {
	AVLTree <Integer, String> t;
	private ByteArrayOutputStream outBuffer = new ByteArrayOutputStream ( ) ;
	private ByteArrayOutputStream errBuffer = new ByteArrayOutputStream ( ) ;
	
	@Before
	public void setupOutputStreams () {
		t = new AVLTree <Integer, String> ();
		System.setOut(new PrintStream (outBuffer)) ;
		System.setErr(new PrintStream (errBuffer)) ;
	}
	
	@After
	public void cleanupOutputStreams ( ) {
		System.setOut(null);
		System.setErr(null);
	}
	
	@Test
	public void testIsAlreadyPresent() {
		t.put(10,"uno");
		t.put(10,"due");
		assertEquals("due",t.get(10));
	}
	
	@Test
	public void testIsEmptyTrue() {
		assertEquals(true,t.isEmpty());
	}
	
	@Test
	public void testIsEmptyFalse() {
		t.put(10,"dieci");
		assertEquals(false,t.isEmpty());
	}
	
	@Test
	public void testPut() {
		t.put(10,"dieci");
		assertEquals(1,t.size());
	}
	
	@Test
	public void testRotateSS() {
		t.put(80,"");
		t.put(90,"");
		t.put(60,"");
		t.put(70,"");
		t.put(40,"");
		t.put(20,"");
		t.put(50,"");
		t.printInOrder();
		String result = "Foglia [key=20, parent=40, h=0, value=]\n"
			+"AVLNode [key=40,left=20, right=50, parent=60, h=1, value=]\n"
			+"Foglia [key=50, parent=40, h=0, value=]\n"
			+"Radice [key=60,left=40, right=80, h=2, value=]\n"
			+"Foglia [key=70, parent=80, h=0, value=]\n"
			+"AVLNode [key=80,left=70, right=90, parent=60, h=1, value=]\n"
			+"Foglia [key=90, parent=80, h=0, value=]\n";
		assertEquals(result, outBuffer.toString());
	}
	
	@Test
	public void testRotateDD() {
		t.put(40,"");
		t.put(30,"");
		t.put(50,"");
		t.put(45,"");
		t.put(60,"");
		t.put(70,"");
		t.put(55,"");
		t.printInOrder();
		String result = "Foglia [key=30, parent=40, h=0, value=]\n"
			+"AVLNode [key=40,left=30, right=45, parent=50, h=1, value=]\n"
			+"Foglia [key=45, parent=40, h=0, value=]\n"
			+"Radice [key=50,left=40, right=60, h=2, value=]\n"
			+"Foglia [key=55, parent=60, h=0, value=]\n"
			+"AVLNode [key=60,left=55, right=70, parent=50, h=1, value=]\n"
			+"Foglia [key=70, parent=60, h=0, value=]\n";
		assertEquals(result, outBuffer.toString());
	}
	
	@Test
	public void testRotateSD() {
		t.put(50,"");
		t.put(40,"");
		t.put(55,"");
		t.put(30,"");
		t.put(45,"");
		t.put(53,"");
		t.put(59,"");
		t.put(20,"");
		t.put(42,"");
		t.put(47,"");
		t.put(43,"");
		t.printInOrder();
		String result = "Foglia [key=20, parent=30, h=0, value=]\n"
			+"AVLNode [key=30,left=20, right=null, parent=40, h=1, value=]\n"
			+"AVLNode [key=40,left=30, right=42, parent=45, h=2, value=]\n"
			+"AVLNode [key=42,left=null, right=43, parent=40, h=1, value=]\n"
			+"Foglia [key=43, parent=42, h=0, value=]\n"
			+"Radice [key=45,left=40, right=50, h=3, value=]\n"
			+"Foglia [key=47, parent=50, h=0, value=]\n"
			+"AVLNode [key=50,left=47, right=55, parent=45, h=2, value=]\n"
			+"Foglia [key=53, parent=55, h=0, value=]\n"
			+"AVLNode [key=55,left=53, right=59, parent=50, h=1, value=]\n"
			+"Foglia [key=59, parent=55, h=0, value=]\n";
		assertEquals(result, outBuffer.toString());
	}
	
	@Test
	public void testRotateDS() {
		t.put(50, "");
		t.put(40, "");
		t.put(70, "");
		t.put(35, "");
		t.put(45, "");
		t.put(60, "");
		t.put(75, "");
		t.put(55, "");
		t.put(65, "");
		t.put(73, "");
		t.put(80, "");
		t.put(63, "");
		t.printInOrder();
		String result = "Foglia [key=35, parent=40, h=0, value=]\n"
			+"AVLNode [key=40,left=35, right=45, parent=50, h=1, value=]\n"
			+"Foglia [key=45, parent=40, h=0, value=]\n"
			+"AVLNode [key=50,left=40, right=55, parent=60, h=2, value=]\n"
			+"Foglia [key=55, parent=50, h=0, value=]\n"
			+"Radice [key=60,left=50, right=70, h=3, value=]\n"
			+"Foglia [key=63, parent=65, h=0, value=]\n"
			+"AVLNode [key=65,left=63, right=null, parent=70, h=1, value=]\n"
			+"AVLNode [key=70,left=65, right=75, parent=60, h=2, value=]\n"
			+"Foglia [key=73, parent=75, h=0, value=]\n"
			+"AVLNode [key=75,left=73, right=80, parent=70, h=1, value=]\n"
			+"Foglia [key=80, parent=75, h=0, value=]\n";
		assertEquals(result, outBuffer.toString());
	}
	
	@Test
	public void testGet() {
		t.put(50, "ciao");
		t.put(40, "mitico");
		t.put(70, "afh");
		t.put(35, "anog");
		t.put(45, "sabf");
		t.put(60, "ajof");
		t.put(75, "jbcòw");
		t.put(55, "nia");
		assertEquals("sabf",t.get(45));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testGetNotExisting() {
		assertEquals("sabf",t.get(100));
	}
	
	@Test
	public void testSize() {
		t.put(55, "nia");
		assertEquals(1,t.size());
	}
	
	@Test
	public void testSizeRemove() {
		t.put(55, "");
		t.put(30, "");
		t.put(45, "");
		t.put(95, "");
		t.remove(45);
		assertEquals(3,t.size());
	}
	
	@Test
	public void testSizeVoid() {
		assertEquals(0,t.size());
	}
	
	@Test
	public void testRemoveHalfWay() {
		t.put(30,"");
		t.put(20,"");
		t.put(40,"");
		t.put(10,"");
		t.put(25,"");
		t.put(35,"");
		t.put(45,"");
		t.put(5,"");
		t.put(11,"");
		t.put(22,"");
		t.put(29,"");
		t.put(31,"");
		t.put(36,"");
		t.put(43,"");
		t.put(47,"");
		t.remove(40);
		t.printInOrder();
		String result = "Foglia [key=5, parent=10, h=0, value=]\n"
			+"AVLNode [key=10,left=5, right=11, parent=20, h=1, value=]\n"
			+"Foglia [key=11, parent=10, h=0, value=]\n"
			+"AVLNode [key=20,left=10, right=25, parent=30, h=2, value=]\n"
			+"Foglia [key=22, parent=25, h=0, value=]\n"
			+"AVLNode [key=25,left=22, right=29, parent=20, h=1, value=]\n"
			+"Foglia [key=29, parent=25, h=0, value=]\n"
			+"Radice [key=30,left=20, right=43, h=3, value=]\n"
			+"Foglia [key=31, parent=35, h=0, value=]\n"
			+"AVLNode [key=35,left=31, right=36, parent=43, h=1, value=]\n"
			+"Foglia [key=36, parent=35, h=0, value=]\n"
			+"AVLNode [key=43,left=35, right=45, parent=30, h=2, value=]\n"
			+"AVLNode [key=45,left=null, right=47, parent=43, h=1, value=]\n"
			+"Foglia [key=47, parent=45, h=0, value=]\n";
		assertEquals(result, outBuffer.toString());
	}
	
	@Test
	public void testRemoveLeaf() {
		t.put(30,"");
		t.put(20,"");
		t.put(40,"");
		t.put(10,"");
		t.put(25,"");
		t.put(35,"");
		t.put(45,"");
		t.put(5,"");
		t.put(11,"");
		t.put(22,"");
		t.put(29,"");
		t.put(31,"");
		t.put(36,"");
		t.remove(45);
		t.printInOrder();
		String result = "Foglia [key=5, parent=10, h=0, value=]\n"
			+"AVLNode [key=10,left=5, right=11, parent=20, h=1, value=]\n"
			+"Foglia [key=11, parent=10, h=0, value=]\n"
			+"AVLNode [key=20,left=10, right=25, parent=30, h=2, value=]\n"
			+"Foglia [key=22, parent=25, h=0, value=]\n"
			+"AVLNode [key=25,left=22, right=29, parent=20, h=1, value=]\n"
			+"Foglia [key=29, parent=25, h=0, value=]\n"
			+"Radice [key=30,left=20, right=40, h=3, value=]\n"
			+"Foglia [key=31, parent=35, h=0, value=]\n"
			+"AVLNode [key=35,left=31, right=36, parent=40, h=1, value=]\n"
			+"Foglia [key=36, parent=35, h=0, value=]\n"
			+"AVLNode [key=40,left=35, right=null, parent=30, h=2, value=]\n";
		assertEquals(result, outBuffer.toString());
	}
	
	@Test
	public void testRemoveRoot() {
		t.put(30,"");
		t.put(20,"");
		t.put(40,"");
		t.put(10,"");
		t.put(25,"");
		t.put(35,"");
		t.put(45,"");
		t.remove(30);
		t.printInOrder();
		String result = "Foglia [key=10, parent=20, h=0, value=]\n"
			+"AVLNode [key=20,left=10, right=25, parent=35, h=1, value=]\n"
			+"Foglia [key=25, parent=20, h=0, value=]\n"
			+"Radice [key=35,left=20, right=40, h=2, value=]\n"
			+"AVLNode [key=40,left=null, right=45, parent=35, h=1, value=]\n"
			+"Foglia [key=45, parent=40, h=0, value=]\n";
		assertEquals(result, outBuffer.toString());
	}
}