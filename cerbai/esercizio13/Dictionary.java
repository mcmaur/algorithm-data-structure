package cerbai.esercizio13;

public interface Dictionary <K extends Comparable <K>, V> {
	/*
	∗ restituisce il valore associato alla
	∗ chiave key ( o null se la chiave non è
	∗ presente )
	*/
	V get (K key);
	
	/*
	∗ restituisce true se il
	* dizionario è vuoto
	*/
	boolean isEmpty ();
	
	/*
	∗ associa il valore value alla chiave
	∗ key e restituisce il valore precedentemente
	∗ associato a key ( o null se la chiave non
	∗ era associata a nessun valore )
	*/
	V put (K key , V value );
	
	/*
	∗ rimuove la chiave key dal dizionario e
	∗ restituisce il valore a essa associata (o null se la
	∗ chiave non e rapresente nel dizionario )
	*/
	V remove (K key );
	
	/*
	∗ restituisce il numero di coppie chiave / valore attualmente
	∗ gestite dal dizionario
	*/
	int size ();
}
