﻿package cerbai.esercizio08;

public interface PriorityQueue {
	public void insert(String e, double p);
	public String first();
	public String extractFirst();
	public boolean isEmpty();
	public void changePriority(int i, double p);
}