package cerbai.esercizio08.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio08.Heap;

public class HeapTest {
	Heap hp;
	private ByteArrayOutputStream outBuffer = new ByteArrayOutputStream ( ) ;
	private ByteArrayOutputStream errBuffer = new ByteArrayOutputStream ( ) ;
	
	@Before
	public void setupOutputStreams () {
		hp = new Heap(10);
		System.setOut(new PrintStream (outBuffer)) ;
		System.setErr(new PrintStream (errBuffer)) ;
	}
	
	@After
	public void cleanupOutputStreams ( ) {
		System.setOut(null);
		System.setErr(null);
	}
	
	@Test
	public void testMoveUp() {
		hp.insert("telefono", 3);
		hp.insert("citofono", 1);
		assertEquals(hp.first(),"citofono");
	}

	@Test
	public void testMoveDown() {
		hp.insert("citofono", 2);
		hp.insert("telefono", 3);
		hp.changePriority(2, 1);
		assertEquals(hp.first(),"telefono");
	}

	@Test
	public void testInsert() {
		hp.insert("telefono", 3);
		hp.insert("citofono", 1);
		hp.insert("campanello", 6);
		hp.insert("computer", 9);
		assertEquals(4, hp.indiceUltimo);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testInsertElementNull() {
		hp.insert(null, 3);
		assertEquals(1, hp.indiceUltimo);
	}

	@Test
	public void testFirst() {
		hp.insert("telefono", 3);
		hp.insert("citofono", 1);
		hp.insert("campanello", 6);
		hp.insert("computer", 9);
		assertEquals("citofono", hp.first());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFirstEmpty() {
		hp.first();
	}

	@Test
	public void testEXtractFirst() {
		hp.insert("telefono", 3);
		hp.insert("citofono", 1);
		hp.insert("campanello", 6);
		hp.insert("computer", 9);
		hp.extractFirst();
		assertEquals(3, hp.indiceUltimo);
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testEXtractFirstEmpty() {
		hp.extractFirst();
	}

	@Test
	public void testIsEmptyTrue() {
		assertEquals(true,hp.isEmpty());
	}
	
	@Test
	public void testIsEmptyFalse() {
		hp.insert("telefono", 3);
		assertEquals(false,hp.isEmpty());
	}

	@Test
	public void testChangePriority() {
		hp.insert("telefono", 3);
		hp.changePriority(1,1.0);
		assertEquals(1.0,hp.elements[1].prior,0);
	}

}
