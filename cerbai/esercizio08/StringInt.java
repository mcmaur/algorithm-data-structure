package cerbai.esercizio08;

public class StringInt implements Comparable <Object>{
	public String elem;
	public double prior;
	
	public StringInt(String elem, double prior) {
		this.elem = elem;
		this.prior = prior;
	}
	
	@Override
	public String toString() {
		return "StringInt [elem=" + elem + ", prior=" + prior + "]";
	}

	@Override
	public int compareTo(Object arg0) {
		if(arg0.getClass() != this.getClass())
			throw new IllegalArgumentException();
		Double db1 = new Double (this.prior);
		StringInt stI = (StringInt)arg0;
		Double db2 = new Double (stI.prior);
		return db1.compareTo(db2);
	}
}
