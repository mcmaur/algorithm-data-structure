package cerbai.esercizio08;

import java.util.Arrays;

public class Heap implements PriorityQueue {
	
	public StringInt [] elements;
	public int indiceUltimo;
	
	/**
	 * Costruttore
	 * @param capacita - lunghezza dell'array degli elementi
	 */
	public Heap(int capacita) {
		if (capacita <= 0) throw new IllegalArgumentException();
		elements = new StringInt [capacita];
	}

	private void moveUp (int i) {
		if(i > indiceUltimo || i <= 0) throw new IllegalArgumentException();
		//salvo in temporaneo l'elemento in questione
		StringInt tempElemento =  elements[i];
		//ciclo fino a che la priorità del padre è maggiore di quella del figlio
		while(i > 1 && tempElemento.prior < elements[i/2].prior) {
			elements[i] = elements[i/2];
			i = i / 2;
		}
		//inserisco l'elemento oggetto della moveUp nella posizione corretta
		elements[i] = tempElemento;
	}
	
	private void moveDown(int i) {
		if(i > indiceUltimo || i <= 0) throw new IllegalArgumentException();
		//salvo in temporaneo l'elemento in question
		StringInt tempElemento = elements[i];
		int j;
		//ciclo fino a che esiste un figlio
		while((j = 2 * i) <= indiceUltimo) {
			//se il secondo figlio dell'elemento in questione è di priorità minore del primo figlio
			if(j + 1 <= indiceUltimo && elements[j + 1].prior < elements[j].prior)
				j++;
			//se ho trovato il posto giusto
			if(tempElemento.prior <= elements[j].prior) break;
			elements[i] = elements[j];
			i = j;
		}
		//inserisco l'elemento oggetto della moveUp nella posizione corretta
		elements[i] = tempElemento;
	}
	
	/**
	 * inserisce l'elemento nell'array previa eventualmente allungamento della struttura dati
	 * @param e - stringa indicante il valore del nuovo elemento
	 * @param p - priorità del nuovo elemento
	 */
	@Override
	public void insert(String e, double p) {
		if(e == null) throw new IllegalArgumentException();
		if(indiceUltimo == elements.length - 1)
			rialloca();
		elements[++indiceUltimo] = new StringInt(e, p);
		moveUp(indiceUltimo);
	}
	
	private void rialloca() {
		StringInt [] newTempArray = new StringInt [(elements.length * 2)];
		for(int i = 0; i < elements.length; i++)
			newTempArray[i] = elements[i];
		elements = newTempArray;
	}

	/**
	 * ritorna il valore del primo elemento
	 * @param la stringa del valore del primo elemento
	 */
	@Override
	public String first() {
		if(indiceUltimo == 0) throw new IllegalArgumentException();
		return  elements[1].elem;
	}

	/**
	 * ritorna il valore del primo elemento e lo elimina dall'array
	 * @return la stringa del valore del primo elemento
	 */
	@Override
	public String extractFirst() {
		if(indiceUltimo == 0) throw new IllegalArgumentException();
		StringInt tmpPrimo = elements[1];
		StringInt tmpUltimo = elements[indiceUltimo];
		elements[indiceUltimo--] = null;
		if(indiceUltimo > 0) {
			elements[1] = tmpUltimo;
			moveDown(1);
		}
		return tmpPrimo.elem;
	}

	/**
	 * verifica se la struttura dati è vuota o meno
	 * @return true se vuota, false altrimenti
	 */
	@Override
	public boolean isEmpty() {
		return indiceUltimo == 0 ? true : false;
	}


	/**
	 * cambia la priorità all'elemento indicato e lo sposta nella corretta posizione
	 * @param i - indice elemento di cui cambiare priorità
	 * @param p - nuova priorità da assegnarvi
	 */
	@Override
	public void changePriority(int i, double p) {
		if(i < 0 || i > indiceUltimo) throw new IllegalArgumentException();
		elements[i].prior = p;
		moveDown(i);
		moveUp(i);
	}
	
	@Override
	public String toString() {
		return "Heap [elements=" + Arrays.toString(elements)
				+ ", indiceUltimo=" + indiceUltimo + "]";
	}

}