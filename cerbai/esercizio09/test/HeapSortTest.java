package cerbai.esercizio09.test;

import cerbai.esercizio05.SortingAlgorithm;
import cerbai.esercizio05.test.SortingAlgorithmTest;
import cerbai.esercizio09.HeapSort;

public class HeapSortTest extends SortingAlgorithmTest{

	@Override
	public SortingAlgorithm createIstance() {
		return new HeapSort();
	}

}