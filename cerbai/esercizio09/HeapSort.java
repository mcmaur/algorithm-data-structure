package cerbai.esercizio09;

import cerbai.esercizio05.SortingAlgorithm;

public class HeapSort implements SortingAlgorithm {

	/**
	 * Provvede ad eseguire l'algoritmo di ordinamento heapsort sull'array dato
	 * @param a - array su cui effettuare l'ordinamento
	 */
	@Override
	public void sort(int[] a) {
		if (a == null) throw new IllegalArgumentException();
		if (a.length == 0) return;
		int indiceUltimo = a.length - 1;
		for(int j = (indiceUltimo - 1) / 2 ; j >= 0; j--)
			moveDown(a,j,a.length);
		for(int i = a.length - 1; i > 0; i--) {
			swap(a, 0, i);
			moveDown(a, 0, i);
		}
			
	}
	
	private void moveDown(int[] a, int i, int lung) {
		if(i > lung || i < 0) throw new IllegalArgumentException();
		//salvo in temporaneo l'elemento in questione
		int tempElemento = a[i];
		int j;
		//ciclo fino a che esiste un figlio
		while((j = 2 * i + 1) < lung) {
			//se il secondo figlio dell'elemento in questione è di priorità maggiore del primo figlio
			if(j + 1 < lung && a[j + 1] > a[j]) j++;
			//se ho trovato il posto giusto
			if(tempElemento >= a[j]) break;
			a[i] = a[j];
			i = j;
		}
		//inserisco l'elemento oggetto della moveUp nella posizione corretta
		a[i] = tempElemento;
	}

	/**
	  * Provvede ad eseguire lo scambio di due elemnti all interno dell array
	  * @param arr - array su cui fare lo scambio
	  * @param i - indice elemento da scambiare
	  * @param j - indice elemento da scambiare
	  */
	 private void swap(int [] arr,int i,int j){
		 if(i < 0 || i > arr.length - 1 || j < 0 || j > arr.length - 1) return;
		 int temp = arr[i];
		 arr[i] = arr[j];
		 arr[j] = temp;
	 }
	
}
