package cerbai.esercizio12;

import java.util.Arrays;

public class UnionFind {
	public int [] parent;
	int[] rank;
	
	/**
	 * Crea la struttura dati e imposta la capacità a n ∗/
	 * @param n - capacità della struttura dati
	 */
	public UnionFind ( int n ) {
		parent = new int[n];
		rank = new int[n];
	    for (int i = 0; i < n; i++) parent[i] = i;
	}

	/**
	 * Getter capacità
	 * @return la capacità della struttura dati
	 */
	public int getCapacity() {
		return parent.length;
	}
	
	/**
	 * Cambia la capacità della struttura dati
	 * @param newN - nuova capacità
	 * @throws IllegalArgumentException se la nuova capacità  minore della precedente
	 */
	public void setCapacity ( int newN) throws IllegalArgumentException {
		if(newN < parent.length) throw new IllegalArgumentException();
		int [] newParent = new int[newN];
		for(int i = 0; i < parent.length; i++)
			newParent[i] = parent [i];
		parent = newParent;
		int [] newRank = new int[newN];
		for(int i = 0; i < rank.length; i++)
			newRank[i] = rank [i];
		rank = newRank; 
	}

	/**
	 * Restituisce il rappresentante dell'insieme di e
	 */
	public int find(int e) {
		int p = parent[e];
		if (e == p) return e;
		return parent[e] = find(p);
	}
	
	/**
	 * Unisce l'insieme gli insime che contengono a e b.
	 * @param a
	 * @param b
	 * @return true se a e b NON erano nello stesso insieme, false altrimenti
	 */
	public boolean union ( int a , int b ) {
		int rapprA = find(a);
		int rapprB = find(b);
		if (rapprB == rapprA) return false;
		if (rank[rapprA] > rank[rapprB]) {
			parent[rapprB] = rapprA;
		} else if (rank[rapprB] > rank[rapprA]) {
			parent[rapprA] = rapprB;
		} else {
			parent[rapprB] = rapprA;
			rank[rapprA]++;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return "UnionFind [parent=" + Arrays.toString(parent) + ", rank="
				+ Arrays.toString(rank) + "]";
	}	
}
