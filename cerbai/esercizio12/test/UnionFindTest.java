package cerbai.esercizio12.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio12.UnionFind;

public class UnionFindTest {
	UnionFind uf;
	private ByteArrayOutputStream outBuffer = new ByteArrayOutputStream ( ) ;
	private ByteArrayOutputStream errBuffer = new ByteArrayOutputStream ( ) ;
	
	@Before
	public void setupOutputStreams () {
		uf = new UnionFind(10);
		System.setOut(new PrintStream (outBuffer)) ;
		System.setErr(new PrintStream (errBuffer)) ;
	}
	
	@After
	public void cleanupOutputStreams ( ) {
		System.setOut(null);
		System.setErr(null);
	}

	@Test
	public void testGetCapacity() {
		assertEquals(10, uf.getCapacity());
	}

	@Test
	public void testSetCapacity() {
		uf.setCapacity(11);
		assertEquals(11, uf.getCapacity());
	}

	@Test
	public void testFind() {
		uf.union(1, 2);
		assertTrue(uf.parent[2] == 1 || uf.parent[1] == 2);
	}
	
	@Test
	public void testFindAfterMoreUnion() {
		uf.union(1, 2);
		uf.union(2, 2);
		uf.union(3, 2);
		assertTrue(uf.parent[2] == 1 || uf.parent[1] == 2);
	}

	@Test
	public void testUnion() {
		uf.union(5, 8);
		assertTrue(uf.parent[5] == 8 || uf.parent[8] == 5);
	}

}
