package cerbai.esercizio04;

import static java.lang.Math.max;

public class BinaryTree {

	protected class Node {
    protected Integer element;
    protected Node left, right;

    Node(int element) {
      this.element = element;
      left = right = null;
    }

    Node(int element, Node left, Node right) {
      this.element = element;
      this.left = left;
      this.right = right;
    }

    boolean isLeaf() {
      return left == null && right == null;
    }
  }
  
  /** abbozzo di classe, messa solo
   *  per poter realizzare il metodo find,
   *  non usata -- come si potrebbe --
   *  negli altri esercizi
   *  
   * @author elio
   *
   */
  public class NodeReference {
    private Node node;
    
    private NodeReference(Node node) {
      this.node = node;
    }
    
    public int getElement() {
      return node.element;
    }
    
    public void setElement(int e) {
      node.element = e;
    }
  }

  protected Node root;

  public BinaryTree() {
    root = null;
  }

  public void display() {
    display(root, 0);
  }

  protected void display(Node node, int k) {
    if(node != null) {
      display(node.right, k+1);
      for(int i = 0; i < k; i++) System.out.print("   ");
      System.out.println(node.element);
      display(node.left, k+1);
    }
  }

  /**
   * Verifico se l'albero è vuoto
   * @return true se l'albero è vuoto, false altrimenti
   */
  public boolean isEmpty() {
    return root == null;
  }

  /**
   *Inserisce elem nell'albero t nel punto specificato dalla stringa path
   *@param elem - l'elemento da inserire
   *@param path - Stringa indicante la posizione dell'inserimento
   */
  public void add(int element, String path) {
    root = add(element, path, root);
  }

  private Node add(int elem, String path, Node nd) {
	  if (nd == null) return new Node(elem);
	  if(path.length() == 0) nd.element = elem;
	  else {
		  switch(path.charAt(0)){
		  case 'L':
			  nd.left = add(elem,path.substring(1),nd.left);
			  break;
		  case 'R':
			  nd.right = add(elem,path.substring(1),nd.right);
			  break;
		  default:
			  throw new IllegalArgumentException("Character not supported");
		  }
	  }
	  return nd;
  }

  /**
   * Stampa l'albero in preorder sulla consolle
   */
  public void printPreOrder() {
    if(root == null) System.out.println("albero vuoto");
    printPreOrder(root);
  }

  private void printPreOrder(Node node) {
	  if(node == null) return;
	  System.out.print(node.element+";");
	  printPreOrder(node.left);
	  printPreOrder(node.right);
  }

  /**
   * Stampa l'albero in inorder sulla consolle
   */
  public void printInOrder() {
	  if(root == null) System.out.println("albero vuoto");
	  printInOrder(root);
  }

  private void printInOrder(Node node) {
	  if(node == null) return;
	  printInOrder(node.left);
	  System.out.print(node.element+";");
	  printInOrder(node.right);
  }

  /**
   * Stampa l'albero in postorder sulla consolle
   * @param t - l'albero di cui stampare i nodi
   */
  public void printPostOrder() {
	  if(root == null) System.out.println("albero vuoto");
	  printPostOrder(root);
  }

  protected void printPostOrder(Node node) {
	  if(node == null) return;
	  printPostOrder(node.left);
	  printPostOrder(node.right);
	  System.out.print(node.element+";");
  }

  /**
   * Restituisce l'altezza dell'albero
   * @return il valore dell'altezza, -1 se vuoto
   */
  public int height() {
    return height(root);
  }

  protected int height(Node node) {
	  return node == null?  -1 : 1+Math.max(height(node.right),height(node.left));
  }
 
  /**
   * Restituisce la somma dei valori di tutti gli elementi
   * @return la somma dei valori dei nodi, 0 se vuoto
   */
  public int sum() {
	  return sum(root);
  }

  private int sum(Node nd) {
    return nd == null? 0 : sum(nd.left)+sum(nd.right)+nd.element;
  }

  /**
   * Restituisce il numero di nodi di t
   * @return il numero di nodi, 0 se vuoto
   */
  public int size() { 
    return size(root);
  }

  private int size(Node nd) {
	  return nd == null? 0 : 1+size(nd.left)+size(nd.right);
  }

  /**
   * Restituisce il numero delle foglie
   * @return il numero di foglie, 0 se l'albero è null
   */
  public int numberOfLeaves() {
	if(root == null) return 0;
    return numberOfLeaves(root);
  }

  private int numberOfLeaves(Node nd) {
    return nd.isLeaf()? 1 : numberOfLeaves(nd.right) + numberOfLeaves(nd.left);
  }

  /**
   * Verifico la presenza dell'elemento x nell'albero
   * @param x - elemento da cercare
   * @return true se l'elemento è presente, false altrimenti
   */
  public boolean search(int x) {
	  return search(x,root);
  }

  /** realizzare con una sola istruzione */
  protected boolean search(int x, Node nd) {
	  return nd == null? false : x == nd.element || search(x,nd.left) || search(x,nd.right);
  }
  
  /**
   * modifica l'albero facendolo diventare il suo speculare
   */
  public void mirrorInPlace() {
	  mirrorInPlace(root);
  }

  protected void mirrorInPlace(Node node) {
	  if(node == null) return;
	  Node tmp = node.left;
	  node.left = node.right;
	  node.right = tmp;
	  mirrorInPlace(node.left);
	  mirrorInPlace(node.right);
  }

  /** 
   * Modifica l'albero incrementando di 1 i valori di tutti gli elementi
   */
  public void increment() {
	  increment(root);
  }

  private void increment(Node node) {
	  if(node == null) return;
	  node.element = node.element+1;
	  increment(node.left);
	  increment(node.right);
  }

  /**
   * Costruisce un nuovo albero uguale fino al livello h-1 incluso
   * @param h - altezza alla quale effettuare il troncamento
   * @return nuovo albero simile a t ma troncato al livello h
   */
  public BinaryTree trimmed(int h) {
	  BinaryTree bn = new BinaryTree();
	  bn.root = trimmed (h,root);
	  return bn;
  }

  /* realizzare con una sola istruzione */
  protected Node trimmed(int h, Node nd) {
    return nd == null || h == 0 ? null : new Node (nd.element, trimmed(h-1, nd.left), trimmed(h-1, nd.right));
  }
  
  /**
   * Modifica l'albero eliminando tutti i nodi di livello >= h
   * @param h - altezza alla quale troncare
   * @return l'albero t modificato
   */
  public void trim(int h) {
	if(h <= 0) root = null;
	else trim1(h, root);
  }
  
 // PRECOND: h >= 1
  protected void trim1(int h, Node nd) {
	  if(nd == null) return;
      if(h == 1){
    	  nd.left = null;
    	  nd.right = null;
    	  return;
      }
      trim1(h-1, nd.left);
      trim1(h-1, nd.right);
      
  }

  /**
   * Modifica l'albero eliminando tutti i nodi di livello >= h
   * @param h - altezza alla quale troncare
   * @return l'albero t modificato
   */
  public void trimTextBookVersion(int h) {
	root = trim(h, root);
  }

  private Node trim(int h, Node nd) {
	  if(nd == null || h == 0) return null;
	  nd.left = trim(h-1, nd.left);
	  nd.right = trim(h-1, nd.right);
	  return nd;
  }

  /**
   * costruisce un nuovo albero speculare dell'albero this
   * @return nuovo albero specchiato di t, null se t è null
   */
  public BinaryTree mirror() {
	  BinaryTree b = new BinaryTree();
	  b.root = mirror(this.root);
	  return b;
  }
  
  private Node mirror(Node nd) {
	  if(nd == null) return null;
	  Node n = new Node(nd.element);
	  n.right = mirror(nd.left);
	  n.left = mirror(nd.right);
	  return n;
  }

  /**
   * Verifico se this e t hanno la stessa struttura e gli stessi valori
   * @param t - albero da confrontare
   * @return true se sono uguali, false altrimenti
   */
  public boolean equalTo(BinaryTree t) {
	  if(t == null) return false;
	  return areEqual(this.root, t.root);
  }

  /**
   * Verifico se this e l'oggetto ob hanno la stessa struttura e gli stessi valori
   * @param t - oggetto da confrontare
   * @return true se sono uguali, false altrimenti
   */
  public boolean equals(Object ob) {
    if(ob == null) return false;
    if(getClass() != ob.getClass()) return false;
    return areEqual(this.root, ((BinaryTree)ob).root);
  }

  protected boolean areEqual(Node node1, Node node2) {
    return node1 == node2 || node1 != null && node2 != null && node1.element == node2.element 
    		&& areEqual(node1.left, node2.left) && areEqual(node1.right, node2.right);
  }

  /**
   * Crea e restituisce un nuovo albero copia di t 
   * @return nuovo albero copia di this
   */
  public BinaryTree copy() {
	  BinaryTree t = new BinaryTree();
	  t.root = copy(root);
	  return t;
  }
  
  protected Node copy(Node nd) {
    return nd == null? null : new Node (nd.element, copy(nd.left), copy(nd.right));
  }  

  private Node find(int x, Node nd) {
    if(nd == null || nd.element == x) return nd;
    Node ris = find(x, nd.left);
    if(ris == null) ris = find(x, nd.right);
    return ris;
  }
  
  /**
   * Restituisce un sottoalbero avente come radice x
   * @param x - elemento radice del sottoalbero restituito
   * @return sottoalbero di radice x, null se l'elemento non è presente nell'albero
   */
  public NodeReference find(int x) {
    return new NodeReference(find(x, root));
  }

  /**
   * Verifico che l'albero è completamente bilanciato
   * @return true se è bilanciato, false altrimenti
   */
  public boolean isCompletelyBalanced() {
	  return isCB(root) != -2;
  }

  private int isCB(Node nd) {
	  if(nd == null) return -1;
	  int l = isCB(nd.left);
	  int r = isCB(nd.right);
	  if(l == -2 || r == -2 || l-r != 0) return -2;
	  return l+1;
  }
  
  /**
   * Verifica se l'albero è "1-bilanciato",
   * @return true se è 1-bilanciato, false altrimenti
   */
  public boolean is1Balanced() {
    return is1Balanced(root) != -2;
  }  

  private int is1Balanced(Node node) {
	  if(node == null) return -1;
	  int l = isCB(node.left);
	  int r = isCB(node.right);
	  if(l == -2 || r == -2 || Math.abs(l-r) > 1) return -2;
	  return l+1;
  }

  /**
   * Restituisce il numero di nodi al livello liv
   * @param t - l'albero si cui effettuare il calcolo
   * @return il numero di foglie, 0 se l'albero è null
   */
  public int numNodesAtLevel(int liv) {
    return numNodesAtLevel(root, liv);
  }

  protected int numNodesAtLevel(Node nd, int lev) {
	  if(nd == null || lev < 0) return 0;
	  if(lev == 0) return numNodesAtLevel(nd.left,lev-1) + numNodesAtLevel(nd.right,lev-1) + 1;
	  else return numNodesAtLevel(nd.left,lev-1) + numNodesAtLevel(nd.right,lev-1);
  }


  private class BoolNode {
    boolean found;
    Node node;

    @SuppressWarnings("unused")
	BoolNode(boolean found, Node node) {
      this.found = found;
      this.node = node;
    }
  }

/** Esercizio opzionale:
   elimina il sottoalbero di radice x;
   se l'elemento x e' presente piu' volte,
   elimina uno solo dei sottoalberi di radice x
   (il primo che trova);
   se l'eliminazione e' stata effettuata,
   restituisce true;
   se invece l'elemento x non e' presente,
   restituisce false
 */
  public boolean removeSubtree(int x) {
    BoolNode ris = removeSubtree(x, root);
    root = ris.node;
    return ris.found;
  }

  protected BoolNode removeSubtree(int x, Node nd) {
	  return null;
  }	

  /**
   * Scrive sulla console i nodi-cardine dell'albero
   */
  public void printCentralNodes() {
    central(root,0);
  }

  private int central(Node nd, int h) {
	  if (nd == null) return 0;
	  int cLR = max(central (nd.right, h+1), central (nd.left, h+1));
	  if(cLR == h) System.out.print(nd.element+";");
	  return cLR + 1;
  }

  /**
   * Restituisce il valore massimo degli elementi
   * @return il massimo degli elemente, "albero vuoto" se albero vuoto
   */
  public int maxElem() {
    if(root == null) throw new IllegalStateException("albero vuoto");
    return maxElem(root);
  }
  
  private static int max3(int x, int y, int z) {
    return max(x, max(y, z));
  }
  
  private int maxElem(Node nd) {
	  if (nd == null) return Integer.MIN_VALUE;
	  return max3(nd.element,maxElem(nd.left),maxElem(nd.right));
  }
  
  /* Esercizio 3.13 pag. 99 libro di testo
   * Restituisce il riferimento al nodo (o a uno dei nodi,
   * se ne esistono più d'uno) U tale che:
   * il rapporto fra il numero dei nodi del sottoalbero
   * di radice U (quindi incluso il nodo stesso) e l'altezza (+1)
   * dello stesso sia massimo;
   * in questo esercizio all'altezza si somma il valore 1
   * altrimenti per le foglie il rapporto sarebbe 1/0 = infinito,
   * e l'esercizio sarebbe banale: il risultato sarebbe
   * una qualunque foglia.
   * 
   * Per ragioni di debugging,
   * il metodo può, prima di restituire il riferimento al nodo,
   * scrivere sulla console il valore del rapporto per quel nodo.
   * 
   * NOTA BENE: Si richiede che l'algoritmo sia lineare nel numero
   * dei nodi, visitando quindi l'albero una volta sola.
   * 
   * Si puo' definire una classe privata ausiliaria,
   * analogamente a quanto proposto per qualche esercizio precedente
   * @return
   * 
   * Naturalmente per fare "il vero lavoro"
   * occorre definire un metodo ricorsivo protected o private,
   * richiamato dal metodo pubblico
   */
  /**
   *  Restituisce il riferimento al nodo U tale che il rapporto fra il numero dei nodi del sottoalbero di 
   *  radice U e l'altezza dello stesso sia massimo
   * @return riferimento al nodo U di rapporto massimo
   */
  public NodeReference maxDescendantsHeightRatio() {
	  return new NodeReference(maxDescendantsHeightRatio(root).nd);
  }
  
  private InfoDescendant maxDescendantsHeightRatio(Node node){
	  if(node == null) return new InfoDescendant(null,0,0,0);
	  InfoDescendant l = maxDescendantsHeightRatio(node.left);
	  InfoDescendant r = maxDescendantsHeightRatio(node.right);
	  
	  InfoDescendant me = new InfoDescendant();
	  me.h = max(l.h, r.h) + 1;
	  me.num_nod = l.num_nod + r.num_nod + 1;
	  double mineMDHR = (double) me.num_nod / me.h;
	  if (l.maxDHR > r.maxDHR) me.nd = l.nd;
	  else me.nd = r.nd;
	  System.out.println("Elemento= "+node.element+" e'= "+mineMDHR+" ("+me.num_nod+"/"+me.h+")");
	  if(mineMDHR > Math.max(l.maxDHR, r.maxDHR)) {
		  me.maxDHR = mineMDHR;
		  me.nd = node;
	  }
	  else me.maxDHR = Math.max(l.maxDHR, r.maxDHR);
	  return me;
  }
  
  private class InfoDescendant {
	  Node nd;
	  int h, num_nod;
	  double maxDHR;
	  
	  public InfoDescendant() {}
	  
	  public InfoDescendant(Node n, int height, int num_nd, double mDHR) {
		  nd = n;
		  h = height;
		  num_nod = num_nd;
		  maxDHR = mDHR;
	  }
  }
  
}

