package cerbai.esercizio04.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio04.BinaryTree;
import cerbai.esercizio04.BinaryTreeGUI;

public class BinaryTreeTest {
	BinaryTree albero,alberoVuoto;
	private ByteArrayOutputStream outBuffer = new ByteArrayOutputStream ( ) ;
	private ByteArrayOutputStream errBuffer = new ByteArrayOutputStream ( ) ;
	
	@Before
	public void setupOutputStreams () {
		albero = new BinaryTree();
		albero.add(3,"L");
		albero.add(7,"L");
		albero.add(2,"LL");
		albero.add(1,"LR");
		albero.add(5,"R");
		albero.add(-1,"RL");
		albero.add(2,"RR");
		alberoVuoto = new BinaryTree();
		System.setOut(new PrintStream (outBuffer)) ;
		System.setErr(new PrintStream (errBuffer)) ;
	}
	
	@After
	public void cleanupOutputStreams ( ) {
		System.setOut(null);
		System.setErr(null);
	}
	
	/*************************************/
	public void disegna(BinaryTree input){
		new BinaryTreeGUI("albero",input);
		try {
			Thread.sleep(30000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	/*************************************/

	@Test
	public void testAdd() {
		albero.printPreOrder();
		assertEquals("3;7;2;1;5;-1;2;",outBuffer.toString());
	}
	
	@Test
	public void testAddEmpty() {
		alberoVuoto.add(2,"RR");
		alberoVuoto.printPreOrder();
		assertEquals("2;",outBuffer.toString());
	}
	
	@Test
	public void testPrintPreOrder() {
		albero.printPreOrder();
		assertEquals("3;7;2;1;5;-1;2;",outBuffer.toString());
	}
	
	@Test
	public void testPrintPreOrderEmpty() {
		alberoVuoto.printPreOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}

	@Test
	public void testPrintInOrder() {
		albero.printInOrder();
		assertEquals("2;7;1;3;-1;5;2;",outBuffer.toString());
	}
	
	@Test
	public void testPrintInOrderEmpty() {
		alberoVuoto.printInOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}

	@Test
	public void testPrintPostOrder() {
		albero.printPostOrder();
		assertEquals("2;1;7;-1;2;5;3;",outBuffer.toString());
	}
	
	@Test
	public void testPrintPostOrderEmpty() {
		alberoVuoto.printPostOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}

	@Test
	public void testHeight() {
		assertEquals(2,albero.height());
	}
	
	@Test
	public void testHeightEmpty() {
		assertEquals(-1,alberoVuoto.height());
	}

	@Test
	public void testSum() {
		assertEquals((2+1+7+5+3+2+(-1)),albero.sum());
	}
	
	@Test
	public void testSumEmpty() {
		assertEquals(0,alberoVuoto.sum());
	}

	@Test
	public void testSize() {
		assertEquals(7,albero.size());
	}
	
	@Test
	public void testSizeEmpty() {
		assertEquals(0,alberoVuoto.size());
	}

	@Test
	public void testNumberOfLeaves() {
		assertEquals(4,albero.numberOfLeaves());
	}
	
	@Test
	public void testNumberOfLeavesEmpty() {
		assertEquals(0,alberoVuoto.numberOfLeaves());
	}

	@Test
	public void testSearchValuePresent() {
		assertTrue(albero.search(7));
	}
	
	@Test
	public void testSearchEmpty() {
		assertFalse(alberoVuoto.search(7));
	}

	@Test
	public void testSearchValueNotPresent() {
		assertFalse(albero.search(10));
	}

	@Test
	public void testMirrorInPlace() {
		albero.mirrorInPlace();
		albero.printPreOrder();
		assertEquals("3;5;2;-1;7;1;2;",outBuffer.toString());
	}
	
	@Test
	public void testMirrorInPlaceEmpty() {
		alberoVuoto.mirrorInPlace();
		alberoVuoto.printPreOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}
	
	@Test
	public void testIncrement() {
		albero.increment();
		albero.printPreOrder();
		assertEquals("4;8;3;2;6;0;3;",outBuffer.toString());
	}
	
	@Test
	public void testIncrementEmpty() {
		alberoVuoto.increment();
		alberoVuoto.printPreOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}

	@Test
	public void testTrimmed() {
		BinaryTree b = albero.trimmed(2);
		b.printPreOrder();
		assertEquals("3;7;5;",outBuffer.toString());
	}
	
	@Test
	public void testTrimmedEmpty() {
		BinaryTree b = alberoVuoto.trimmed(2);
		b.printPreOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}

	@Test
	public void testTrim() {
		albero.trim(2);
		albero.printPreOrder();
		assertEquals("3;7;5;",outBuffer.toString());
	}
	
	@Test
	public void testTrimEmpty() {
		alberoVuoto.trim(1);
		alberoVuoto.printPreOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}
	
	@Test
	public void testTrimTextBookVersion() {
		albero.trimTextBookVersion(1);
		albero.printPreOrder();
		assertEquals("3;",outBuffer.toString());
	}
	
	@Test
	public void testTrimTextBookVersionZeroH() {
		albero.trimTextBookVersion(0);
		albero.printPreOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}

	@Test
	public void testMirror() {
		BinaryTree t = albero.mirror();
		t.printPreOrder();
		assertEquals("3;5;2;-1;7;1;2;",outBuffer.toString());
	}
	
	@Test
	public void testMirrorVuoto() {
		BinaryTree alber = new BinaryTree();
		BinaryTree t = alber.mirror();
		t.printPreOrder();
		assertEquals("albero vuoto\n",outBuffer.toString());
	}

	@Test
	public void testEqualToTrue() {
		assertTrue(albero.equalTo(albero));
	}
	
	@Test
	public void testEqualToFalse() {
		BinaryTree alber = new BinaryTree();
		alber.add(3,"L");
		assertFalse(albero.equalTo(alber));
	}

	@Test
	public void testEqualToNull() {
		assertFalse(albero.equalTo(null));
	}
	
	@Test
	public void testEqualsObjectTrue() {
		assertTrue(albero.equals(albero));
	}
	
	@Test
	public void testEqualsObjectFalse() {
		assertFalse(albero.equals(new BinaryTree()));
	}
	
	@Test
	public void testEqualsObjectDifferentClass() {
		Integer i = new Integer (5);
		assertFalse(albero.equals(i));
	}

	@Test
	public void testCopy() {
		BinaryTree bin = albero.copy();
		bin.printPreOrder();
		assertEquals("3;7;2;1;5;-1;2;",outBuffer.toString());
	}

	@Test
	public void testFindTrue() {
		BinaryTree.NodeReference nd = albero.find(3);
		assertEquals(3, nd.getElement());
	}
	
	@Test(expected = NullPointerException.class)
	public void testFindFalse() {
		BinaryTree.NodeReference nd = albero.find(15);
		nd.getElement();
	}
	
	@Test
	public void testIsCompletelyBalancedNull() {
		assertTrue(alberoVuoto.isCompletelyBalanced());
	}
	
	@Test
	public void testIsCompletelyBalancedTrue() {
		assertTrue(albero.isCompletelyBalanced());
	}
	
	@Test
	public void testIsCompletelyBalancedFalse() {
		BinaryTree alber = new BinaryTree();
		alber.add(3,"L");
		alber.add(7,"L");
		alber.add(2,"LL");
		alber.add(1,"LR");
		alber.add(5,"R");
		alber.add(9,"RL");
		assertFalse(alber.isCompletelyBalanced());
	}
	
	@Test
	public void testIs1BalancedVoid() {
		assertTrue(alberoVuoto.is1Balanced());
	}

	@Test
	public void testIs1BalancedCompletelyBalanced() {
		assertTrue(albero.is1Balanced());
	}
	
	@Test
	public void testIs1Balanced1Balanced() {
		BinaryTree alb = new BinaryTree();
		alb.add(3,"L");
		alb.add(7,"L");
		alb.add(2,"LL");
		alb.add(1,"LR");
		alb.add(5,"R");
		assertTrue(alb.is1Balanced());
	}
	
	@Test
	public void testIs1BalancedNot1Balanced() {
		BinaryTree alb = new BinaryTree();
		alb.add(3,"L");
		alb.add(7,"L");
		alb.add(2,"LL");
		alb.add(1,"LR");
		assertFalse(alb.is1Balanced());
	}

	@Test
	public void testNumNodesAtLevelIntOutOfBound() {
		assertEquals(0,albero.numNodesAtLevel(9));
	}
	
	@Test
	public void testNumNodesAtLevelIntRoot() {
		assertEquals(1,albero.numNodesAtLevel(0));
	}
	
	@Test
	public void testNumNodesAtLevelNegative() {
		assertEquals(0,albero.numNodesAtLevel(-4));
	}

	@Test
	public void testPrintCentralNodes() {
		albero.printCentralNodes();
		assertEquals("5;7;",outBuffer.toString());
	}
	
	@Test
	public void testPrintCentralNodesUnbalancedTree() {
		BinaryTree alber = new BinaryTree();
		alber.add(3,"L");
		alber.add(7,"L");
		alber.add(2,"LL");
		alber.add(1,"LR");
		alber.add(5,"R");
		alber.printCentralNodes();
		assertEquals("7;",outBuffer.toString());
	}
	
	@Test
	public void testPrintCentralNodesHeight2() {
		BinaryTree alber = new BinaryTree();
		alber.add(3,"L");
		alber.add(7,"L");
		alber.add(5,"R");
		alber.printCentralNodes();
		assertEquals("",outBuffer.toString());
	}

	@Test
	public void testMaxElem() {
		assertEquals(7,albero.maxElem());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testMaxElemAlberoVuoto() {
		BinaryTree alber = new BinaryTree();
		alber.maxElem();
	}

	@Test
	public void testMaxDescendantsHeightRatio() {
		BinaryTree.NodeReference nr = albero.maxDescendantsHeightRatio();
		assertEquals(3,nr.getElement());
	}
	
	@Test
	public void testMaxDescendantsHeightRatioLongTree() {
		BinaryTree alb = new BinaryTree();
		alb.add(1, "R");
		alb.add(2, "L");
		alb.add(3, "R");
		alb.add(4, "LL");
		alb.add(5, "LLL");
		alb.add(6, "LLLL");
		alb.add(7, "LLLLL");
		alb.add(8, "LLLLLL");
		alb.add(9, "LLLLLR");
		BinaryTree.NodeReference nr = alb.maxDescendantsHeightRatio();
		assertEquals(7,nr.getElement());
	}

}
