package cerbai.esercizio05;

public interface SortingAlgorithm {
	public void sort (int[] x);
}