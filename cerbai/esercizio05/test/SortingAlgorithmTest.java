package cerbai.esercizio05.test;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio05.SortingAlgorithm;

public abstract class SortingAlgorithmTest {
	
	private SortingAlgorithm sortingAlgorithm;
	
	public abstract SortingAlgorithm createIstance();

	@Before
	public void instantiate () {
		sortingAlgorithm = createIstance();
	}
	
	/* Il test evidenzia che il metodo non produce nessuna eccezione 
	 */
	@Test
	public void testSortArrayVuoto() {
		int [] a = new int [0];
		sortingAlgorithm.sort(a);
	}
	
	
	@Test(expected = IllegalArgumentException.class)
	public void testSortArrayNull() {
		sortingAlgorithm.sort(null);
	}
	
	@Test
	public void testSortOrdinato() {
		int [] a = {0,1,2,3,4,5,6,7,8,9};
		sortingAlgorithm.sort(a);
		assertArrayEquals(new int []{0,1,2,3,4,5,6,7,8,9},a);
	}
	
	@Test
	public void testSortNonOrdinato() {
		int [] a = {2,0,1,3,6,4,5,7,9,8};
		sortingAlgorithm.sort(a);
		assertArrayEquals(new int []{0,1,2,3,4,5,6,7,8,9},a);
	}
}
