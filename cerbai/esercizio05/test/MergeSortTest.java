package cerbai.esercizio05.test;

import cerbai.esercizio05.MergeSort;
import cerbai.esercizio05.SortingAlgorithm;


public class MergeSortTest extends SortingAlgorithmTest {
	
	@Override
	public SortingAlgorithm createIstance() {
		return new MergeSort();
	}
}
