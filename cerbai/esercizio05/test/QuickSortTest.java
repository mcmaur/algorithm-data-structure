package cerbai.esercizio05.test;

import cerbai.esercizio05.QuickSort;
import cerbai.esercizio05.SortingAlgorithm;


public class QuickSortTest extends SortingAlgorithmTest{

	@Override
	public SortingAlgorithm createIstance() {
		return new QuickSort();
	}

}