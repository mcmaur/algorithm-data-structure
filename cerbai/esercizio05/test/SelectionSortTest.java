/**
 * 
 */
package cerbai.esercizio05.test;

import cerbai.esercizio05.SelectionSort;
import cerbai.esercizio05.SortingAlgorithm;


public class SelectionSortTest extends SortingAlgorithmTest{
	
	@Override
	public SortingAlgorithm createIstance() {
		return  new SelectionSort();
	}

}
