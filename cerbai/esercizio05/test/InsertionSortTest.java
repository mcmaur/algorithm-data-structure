package cerbai.esercizio05.test;

import cerbai.esercizio05.InsertionSort;
import cerbai.esercizio05.SortingAlgorithm;


public class InsertionSortTest extends SortingAlgorithmTest {

	@Override
	public SortingAlgorithm createIstance() {
		return new InsertionSort();
	}

}
