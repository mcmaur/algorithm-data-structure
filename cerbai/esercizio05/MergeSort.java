package cerbai.esercizio05;

public class MergeSort implements SortingAlgorithm{

	@Override
	public void sort(int[] a) {
		if (a == null) throw new IllegalArgumentException();
		if (a.length == 0) return;
		msort(a, 0, a.length-1);
	}
	
	/**MergeSort
	 * Dato un array in input lo dividiamo in due parti e lo passiamo alla merge
	 * @param a - array sul quale effettuare l'ordinamento
	 * @param first - primo elemento dell'array
	 * @param last - ultimo elemento dell'array
	 * 
	 *INVARIANTE
	 *   first                 last
	 *  |__________________________|
	 */
	private void msort(int[] a,int first,int last) {
		if(first < last){
			//calcolo l'indice della cella a metà
			int iMed = (first + last)/2;
			//richiamo ricorsivamente la procedura
			msort(a, first, iMed);
			msort(a, iMed+1, last);
			//fondo le due meta
			merge(a, first, iMed, last);
		}
	}
	
	/**Merge
	 * Dato "due array" in input li fondiamo in ordine in un terzo array
	 * @param a - array sul quale effettuare l'ordinamento
	 * @param first - primo elemento dell'array
	 * @param mid - elemento centrale che determina la fine del primo array "virtuale" e l'inizio del secondo
	 * @param last - ultimo elemento dell'array
	 * 
	 *INVARIANTE
	 *  first       mid          last
	 * |_______________|_____________|
	 */
	private void merge(int[] a, int fst, int mid, int lst) {
		int n = lst-fst +1, i = fst, j = mid+1, k = 0;
		int [] c = new int [n];
		//cicliamo fino a che uno dei due array in input non arriva alla fine
		while(i <= mid && j <= lst)
			if(a[i] <= a[j]) c[k++] = a[i++];
			else c[k++] = a[j++];
		//copiamo il restante contenuto di a o b in c
		while(i <= mid) c[k++] = a[i++];
		while(j <= lst) c[k++] = a[j++];
		//copiamo il contenuto di c in a
		for(int h = 0; h < n; h++) a[fst + h] = c[h];
	}
}
