package cerbai.esercizio05;

public class InsertionSort implements SortingAlgorithm{

	/**Insertion Sort
	 * Dato un array in input seleziono il valore corrente e lo inserisco al posto corretto
	 * @param a - array sul quale effettuare l'ordinamento
	 * 
	 *INVARIANTE
	 * 0         i                      n-1
	 *|_________|__________________________|
	 *  ordinati          da ordinare
	 */
	@Override
	public void sort(int[] a) {
		if (a == null) throw new IllegalArgumentException();
		if (a.length == 0) return;
		int n = a.length,j;
		//ciclo su tutto l'array tranne il primo elemento che il momento è già al posto giusto
		for(int i = 1; i < n; i++) {
			int x = a[i];
			for(j = i; j > 0; j--) {
				//controllo che tutti i valori precedenti ad a[i] siano minori dello stesso altrimenti mi fermo
				if(x >= a[j-1]) break;
				a[j] = a[j-1];
			}
			//se sono uscito dal ciclo significa che tutti gli elementi minori di j sono minori o che j = 0
			a[j] = x;
		}
	}
}
