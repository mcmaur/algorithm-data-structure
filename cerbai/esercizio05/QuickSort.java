package cerbai.esercizio05;

public class QuickSort implements SortingAlgorithm{

	@Override
	public void sort(int[] a) {
		if (a == null) throw new IllegalArgumentException();
		if (a.length == 0) return;
		qsort(a, 0, a.length-1);
	}
	
	/**
	 * Applicazione dell'algoritmo di sorting QuickSort
	 * @param a - array da ordinare
	 * @param inf - indice primo elemento
	 * @param sup - indice ultimo elemento
	 * 
	 *        inf                  sup
	 * |_____|________________________|__________|
	 */
	private void qsort(int [] a , int inf , int sup) {
		if(inf < sup) {
			//salvo in temporaneo il pivot
			int pivot = a[inf];
			int i = inf;
			//ciclo per la parte centrale da smistare ancora
			for(int j = inf + 1; j <= sup; j++) {
				if(a[j] < pivot) {
					//lo sposto nella prima parte
					i++;
					swap(a, j, i);
				}	
			}
			swap(a, inf, i);
			//rieseguo il quick-sort sulla prima e ultima parte dell'array
			qsort(a, inf, i-1);
			qsort(a, i+1, sup);
		}
	}
	
	/**
	  * Provvede ad eseguire lo scambio di due elemnti all interno dell array
	  * @param arr - array su cui fare lo scambio
	  * @param i - indice elemento da scambiare
	  * @param j - indice elemento da scambiare
	  */
	 private void swap(int [] arr,int i,int j){
		 if(i < 0 || i > arr.length - 1 || j < 0 || j > arr.length - 1) return;
		 int temp = arr[i];
		 arr[i] = arr[j];
		 arr[j] = temp;
	 }

}
