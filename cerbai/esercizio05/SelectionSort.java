package cerbai.esercizio05;

public class SelectionSort implements SortingAlgorithm{

	/**Selection Sort
	 * Dato un array in input seleziono il minimo e lo metto al primo posto non ordinato
	 * @param a - array sul quale effettuare l'ordinamento
	 * 
	 *INVARIANTE
	 * 0         i                      n-1
	 *|_________|__________________________|
	 *  ordinati          da ordinare
	 */
	@Override
	public void sort(int[] a) {
		if (a == null) throw new IllegalArgumentException();
		if (a.length == 0) return;
		int n = a.length;
		//continuo fino a quando non mi rimane un solo elemento che risulterà già ordinato
		for(int i = 0; i < n-1; i++) {
			//cerco il minimo nella parte ancora da ordinare
			int iMin = i;
			for(int j = i+1; j < n; j++)
				if(a[j] < a[iMin]) iMin = j;
			//sistemo il minimo al primo posto non ordinato
			swap(a, i, iMin);
		}
	}
	
	/**
	  * Provvede ad eseguire lo scambio di due elemnti all interno dell array
	  * @param arr - array su cui fare lo scambio
	  * @param i - indice elemento da scambiare
	  * @param j - indice elemento da scambiare
	  */
	 private void swap(int [] arr,int i,int j){
		 if(i < 0 || i > arr.length - 1 || j < 0 || j > arr.length - 1) return;
		 int temp = arr[i];
		 arr[i] = arr[j];
		 arr[j] = temp;
	 }

}
