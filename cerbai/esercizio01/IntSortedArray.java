package cerbai.esercizio01;

public class IntSortedArray {
	private int[] elements;
	private int numElements;
	
	/**
	 * Getter
	 * @return la lunghezza dell'array
	 */
	public int length() {
		return elements.length;
	}
	
	/**
	 * Costruttore
	 * @param initialCapacity - dimensione dell'array
	 */
	public IntSortedArray(int initialCapacity) {
		if (initialCapacity <= 0) throw new IllegalArgumentException();
		elements = new int[initialCapacity];
		numElements = 0;
	}

	/**
	 * Indica il numero degli elementi
	 * @return il numero degli elementi presenti nell'array
	 */
	public int size(){
		return numElements;
	}
	
	/**
	 * Ricerca binaria iterativa versione raffinata
	 * @param x - elemento da cercare
	 * @return -1 se elemento non presente, altrimenti l'indice
	 */
	private int binarySearch(int x){
		int inf = 0, sup = numElements-1;
		//se l'elemento è fuori dal range dei numeri nell'array esci
		if(sup == -1 || x < elements[inf]) return -1;

		if(x > elements[sup]) return -numElements-1;
		
		while(inf <= sup){
			//calcola l'indice dell'elemento a metà
			int med = (inf+sup) >>> 1;
			if (x < elements[med])
				//accorcio la porzione da considerare
				sup = med - 1;
			else if (x > elements[med])
				//accorcio la porzione da considerare
				inf = med + 1;
			//ho trovato l'elemento ed è di indice med
			else return med;
		}
		return -inf-1;
	}
	
	/**
	 * Inserisce elemento x nell'array
	 * @param x - elemento da inserire
	 * @return l'indice dell'inserimento se elemento non ripetuto,-1 altrimenti
	 */
	public int insert(int x){
		//effettuo la ricerca binaria dell'elemento
		int ind = binarySearch(x);
		//se l'elemento è fuori dal range dei numeri possibili (-infinito a 0)
		if(ind >= 0)	return -1;
		//se l'array è pieno rialloca e ricopia
		if(numElements == elements.length){
			int [] tmpArr = new int[numElements*2];
			for(int i = 0; i < numElements; i++)
				tmpArr[i] = elements[i];
			elements=tmpArr;
		}
		//calcolo l'indice reale della posizione dell'elemento
		int indiceReale = -ind-1;
		//sposto tutti gli elementi fino a liberare la posizione necessaria per l'inserimento
		for(int i = numElements; i > indiceReale; i--)
			elements[i] = elements[i-1];
		elements[indiceReale] = x;
		numElements++;
		return indiceReale;
	}
	
	/**
	 * Ritorno elemento indice i nell'array
	 * @param i - indice elemento
	 * @return valore elemento
	 */
	public int get(int i){
		if(i < 0 || i > numElements-1) throw new IllegalArgumentException();
		return elements[i];
	}
	
	/**
	 * Stampo l'array
	 */
	public void print(){
		for(int i = 0; i < numElements; i++)
			 System.out.print(elements[i]+";");
		System.out.println();
	}
}
