package cerbai.esercizio01;

public class BinarySearch {

	private BinarySearch(){
		//avoid the creation of the object
	}

	/**
	 * Controlla se l'elemento x è presente in a
	 * @param x - elemento da cercare
	 * @param a - array dentro il quale cercare l elemento
	 * @return true se l'elemento cercato è presente nell'array, false altrimenti
	 */
	public static boolean isPresent(int x,int []a){
		for(int i=0;i<a.length;i++)
			if (a[i]==x)
				return true;
		return false;
	}
	
	/**
	 * Ricerca binaria versione base iterativa
	 * @param x - elemento da cercare
	 * @param a - array dentro il quale cercare
	 * @return -1 se elemento non presente, altrimenti l'indice
	 */
	public static int find(int x,int []a){
		int inf = 0, sup = a.length - 1;
		//essendo l'array ordinato se non soddisfa le seguenti condizione non è certamente contenuto all'interno
		if(sup == -1 || x < a[inf] || x > a[sup]) return -1;
		while(inf <= sup){
			//calcolo la meta
			int med = (inf+sup)>>>1;
			//imposto come da ricercare nella prima o seconda meta dell'array se l'elemento risulta 
			//	rispettivamente minore dell' elemento con indice med oppure maggiore
			if (x < a[med])
				sup = med - 1;
			else if (x > a[med])
				inf = med + 1;
			//altrimenti l'elemento contenuto in a[med] è proprio quello cercato
			else return med;
		}
		//se sono uscito dal ciclo significa che l'elemento non è presente
		return -1;
	}

	/**
	 * Ricerca binaria ricorsiva
	 * @param x - elemento da cercare
	 * @param a - array dentro il quale cercare l array
	 * @return chiamata al metodo findRecursively(int x,int []a,int inf,int sup)
	 */
	public static int findRecursively(int x,int []a){
		return findRecursively(x,a,0,a.length-1);
	}

	/**
	 * Ricerca binaria ricorsiva
	 * @param x - elemento da cercare
	 * @param a - array dentro il quale cercare l elemento
	 * @param inf - estremo inferiore della porzione di array
	 * @param sup - estremo superiore della porzione di array
	 * @return l'indice di x se presente, altrimenti -1
	 */
	static int findRecursively(int x,int []a,int inf,int sup){
		if(inf > sup) return -1;
		int med = (inf+sup) >>> 1;
		//se l'elemento è minore di a[med] ed esiste allora si trova dentro la prima meta dell'array
		if (x < a[med]) return findRecursively(x, a, inf, med-1);
		if (x > a[med]) return findRecursively(x, a, med+1, sup);
		//altrimenti l'elemento contenuto in a[med] è proprio quello cercato
		return med;
	}

	public static void main (String[]args){
		int [] a = {1,2,3,4,5,7,8,10,13,14};
		System.out.print("Array in input:\n");
		for(int i=0;i<a.length;i++)
			 System.out.print(a[i]+"\t");
		System.out.println();
		System.out.println("Prova uno: metodo find // valore presente // "+find(2,a));
		System.out.println("Prova due: metodo ricorsivo // valore presente // "+findRecursively(8,a));
		System.out.println("Prova tre: metodo find // valore NON presente // "+find(6,a));
		System.out.println("Prova quattro: metodo ricorsivo // valore NON presente // "+findRecursively(11,a));
	}
}
