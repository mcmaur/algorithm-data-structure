package cerbai.esercizio01.test;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import cerbai.esercizio01.IntSortedArray;

public class IntSortedArrayTest {
	
	private final ByteArrayOutputStream outBuffer = new ByteArrayOutputStream ( ) ;
	private final ByteArrayOutputStream errBuffer = new ByteArrayOutputStream ( ) ;
	
	@Before
	public void setupOutputStreams ( ) {
		System.setOut(new PrintStream (outBuffer)) ;
		System.setErr(new PrintStream (errBuffer)) ;
	}
	
	@After
	public void cleanupOutputStreams ( ) {
	System.setOut(null);
	System.setErr(null);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testIntSortedArrayCapacity0() {
			new IntSortedArray(0);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testIntSortedArrayNegativeCapacity() {
			new IntSortedArray(-5);
	}
	
	@Test
	public void testIntSortedArray(){
		IntSortedArray i = new IntSortedArray(3);
		assertEquals(i.length(),3);
	}

	@Test
	public void testSize() {
		IntSortedArray i = new IntSortedArray(3);
		i.insert(1);
		i.insert(2);
		assertEquals(i.size(),2);
	}

	@Test
	public void testInsert() {
		IntSortedArray i = new IntSortedArray(3);
		int a = i.insert(5);
		assertEquals(i.get(a),5);
	}
	
	@Test
	public void testInsertAlreadyPresentValue() {
		IntSortedArray i = new IntSortedArray(3);
		i.insert(5);
		int b = i.insert(5);
		assertEquals(b,-1);
	}

	@Test
	public void testGet() {
		IntSortedArray i = new IntSortedArray(3);
		i.insert(10);
		assertEquals(i.get(0),10);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testGetNegativeValue() {
		IntSortedArray i = new IntSortedArray(3);
		i.get(-5);
	}

	@Test
	public void testPrint() {
		IntSortedArray i = new IntSortedArray(3);
		i.insert(1);
		i.insert(10);
		i.insert(2);
		i.insert(5);
		i.print();
		assertEquals("1;2;5;10;\n",outBuffer.toString());
	}

}
